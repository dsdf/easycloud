import sys
sys.path.append("..")

from core import metaconfmanager
from core import metamanager
from core.measures_sink import Measure
from core.measures_sink.activemq import ActiveMQSink
from core.measures_sink.cassandra import CassandraSink
from core.measures_sink.file import CsvFileSink
from core.measures_sink.kafka import KafkaSink
from core.measures_sink.mongodb import MongoDBSink
from core.measures_sink.prometheus import PrometheusSink
from core.measures_sink.rabbitmq import RabbitMQSink
from core.measures_sink.redis import RedisSink
from datetime import datetime
import logging
import random
import tempfile


class MockConfManager(metaconfmanager.MetaConfManager):
    def __init__(self):
        pass


class MockManager(metamanager.MetaManager):
    TAG = "[MockManager]"
    NUM_IMAGES = 10
    NUM_ZONES = 4
    NUM_INSTANCE_TYPES = 3
    NUM_SECURITY_GROUPS = 2
    NUM_NETWORKS = 2


    def __init__(self, num_images = NUM_IMAGES, num_zones = NUM_ZONES, num_instance_types = NUM_INSTANCE_TYPES, num_security_groups = NUM_SECURITY_GROUPS, num_networks = NUM_NETWORKS):
        self._platform_name = "Mock Platform"
        self._conf = MockConfManager()
        self._setup(num_images, num_zones, num_instance_types, num_security_groups, num_networks)
        self.connect()

    def _setup(self, num_images, num_zones, num_instance_types, num_security_groups, num_networks):
        self._images = [[i, "<image#{} name>".format(i), "<image#{} ID>".format(i), "<image#{} status>".format(i)] for i in range(0,num_images)]
        self._zones = [[i, "<zone#{} name>".format(i), "<zone#{} state>".format(i), "<zone#{} region name>".format(i)] for i in range(0,num_zones)]
        self._instance_types = [[i, "<instance-type#{} ID>".format(i), random.randint(1,8), random.randint(4, 128), random.randint(100, 1000)] for i in range(0,num_instance_types)]
        self._security_groups = [[i, "<securit-group#{} name>".format(i), "<securit-group#{} description>".format(i)] for i in range(0,num_security_groups)]
        self._networks = [[i, "<network#{} name>".format(i), "<network#{} description>".format(i)] for i in range(0,num_networks)]

    def connect(self):
        logging.debug(TAG + " Connected")

    def _platform_list_all_images(self):
        return self._images

    def _platform_list_all_availability_zones(self):
        return self._zones

    def _platform_list_all_instance_types(self):
        return self._instance_types

    def _platform_list_all_security_groups(self):
        return self._security_groups

    def _platform_list_all_networks(self):
        return self._networks




def test_sink(sink, n_instances, n_put, n_mput):
    print("Sink: {}, operation: PUT".format(str(sink)))
    for i in range(0,n_put):
        measure = make_rand_measure(n_instances)
        sink.put(measure)
    print("Press any key")
    input()
    print("Sink: {}, operation: MPUT".format(str(sink)))
    measures = []
    for i in range(0,n_mput):
        measures.append(make_rand_measure())
    sink.mput(measures)
    print("Press any key")
    input()

def test_activemq_sink(n_instances, n_put, n_mput):
    sink = ActiveMQSink()
    test_sink(sink, n_instances, n_put, n_mput)

def test_cassandra_sink(n_instances, n_put, n_mput):
    sink = CassandraSink()
    test_sink(sink, n_instances, n_put, n_mput)

def test_csvfile_sink(n_instances, n_put, n_mput):
    with tempfile.TemporaryDirectory() as basepath:
        print('Base path: {}'.format(basepath))
        sink = CsvFileSink(basepath)
        test_sink(sink, n_instances, n_put, n_mput)

def test_kafka_sink(n_instances, n_put, n_mput):
    sink = KafkaSink()
    test_sink(sink, n_instances, n_put, n_mput)

def test_mondodb_sink(n_instances, n_put, n_mput):
    sink = MongoDBSink()
    test_sink(sink, n_instances, n_put, n_mput)

def test_prometheus_sink(n_instances, n_put, n_mput):
    sink = PrometheusSink()
    test_sink(sink, n_instances, n_put, n_mput)

def test_rabbitmq_sink(n_instances, n_put, n_mput):
    sink = RabbitMQSink()
    test_sink(sink, n_instances, n_put, n_mput)

def test_redis_sink(n_instances, n_put, n_mput):
    sink = RedisSink()
    test_sink(sink, n_instances, n_put, n_mput)

def make_rand_measure(n_instances=10):
    return Measure(object_ns = 'generic',
                   object_id = 'INSTANCE-ID-' + str(random.randint(0, n_instances)),
                   metric = random.choice(['cpu_load', 'memory_free', 'memory_used']),
                   timestamp = datetime.now(),
                   value = random.uniform(0,1),
                   unit = '%')


if __name__ == '__main__':
    num_instances = 10
    num_puts = 100
    num_mputs = 100

    random.seed(5489)
    #test_activemq_sink(num_instances, num_puts, num_mputs)
    #test_cassandra_sink(num_instances, num_puts, num_mputs)
    #test_csvfile_sink(num_instances, num_puts, num_mputs)
    #test_kafka_sink(num_instances, num_puts, num_mputs)
    #test_mongodb_sink(num_instances, num_puts, num_mputs)
    test_prometheus_sink(num_instances, num_puts, num_mputs)
    #test_rabbitmq_sink(num_instances, num_puts, num_mputs)
    #test_redis_sink(num_instances, num_puts, num_mputs)

