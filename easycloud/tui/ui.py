from easycloud.tui.simpletui import SimpleTUI


class ConfManagerUI:
    def __init__(self, conf):
        self.conf_ = conf

    def as_for_data(self, section_name, param_name, return_type=None, regex=None):
        """
        Ask for user input if a parameter is not defined

        Args:
            param_name (str): the name of the missing parameter
            return_type (<return_type>, optional): the return type assigned to user input
            regex (str): the regular expression the input must follow

        Returns:
            <return_type>: user input of type <return_type>
        """
        return SimpleTUI.input_dialog("Missing value",
                                      question="A parameter required by this module has not been defined in settings.cfg!\n"
                                      "Please, define a value to assign to \"" + param_name + "\" (\"" + section_name + "\" section)",
                                      return_type=return_type,
                                      regex=regex,
                                      pause_on_exit=False,
                                      cannot_quit=True)

    def get_parameter(self, section, option, return_type=None, regex=None, default=None):
        """
        Return the parameter stored in the module configuration file or
        ask the user to provide it if no default value is given.

        Args:
            section (str): name of the section (a name surrounded with square brackets
                inside the configuration file).
            option (str): name of the parameter.
            return_type (<return_type>, optional): the return type assigned to
                user input, if not specified in the configuration file.
            regex (str): the regular expression the input must follow, if a user
                input is required.

        Returns:
            <return_type>: user input of type <return_type>.
        """
        value = self.conf_.get_parameter(section,
                                         option,
                                         return_type=return_type,
                                         regex=regex,
                                         default=default)
        if value is None:
            return self.ask_for_data(section, option, regex=regex, return_type=return_type)
