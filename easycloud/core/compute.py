from abc import ABC, abstractmethod
from enum import auto, Enum, unique
import typing
#from libcloud.compute.base import Node, NodeState


#InstanceStatus = NodeState

#Instance = Node


@unique
class InstanceStatus(Enum):
    """ Instance status.  """

    ERROR = 'ERROR'
    MIGRATING = 'MIGRATING'
    NORMAL = 'NORMAL'
    PAUSED = 'PAUSED'
    PENDING = 'PENDING'
    REBOOTING = 'REBOOTING'
    RECONFIGURING = 'RECONFIGURING'
    RUNNING = 'RUNNING'
    STARTING = 'STARTING'
    STOPPED = 'STOPPED'
    STOPPING = 'STOPPING'
    SUSPENDED = 'SUSPENDED'
    TERMINATED = 'TERMINATED'
    UNKNOWN = 'UNKNOWN'
    UPDATING = 'UPDATING'

    @classmethod
    def from_string(cls, value):
        """
        Return the instance of this class that matches the given string.

        Args:
            value (str): the string to match.

        Returns:
            InstanceStatus: the instance of this class that matches the given string.
        """
        try:
            return cls[str(value).upper()]
        except KeyError:
            return None

    def __repr__(self):
        return self.value


class Instance(ABC):
    """ An instance hosted by the cloud infrastructure. """

    #def __init__(self, id, name=None, status=None):
    #    self._id = id
    #    self._name = name if name is not None else id
    #    self._status = status if status is not Node else InstanceStatus.UNKNOWN

    @property
    def extra(self):
        return {}

    @property
    @abstractmethod
    def handle(self):
        """ Returns the low-level handle of this instance. """
        pass

    @property
    @abstractmethod
    def id(self):
        #return self._id
        pass

    @property
    @abstractmethod
    def name(self):
        #return self._name
        pass

    @property
    @abstractmethod
    def status(self):
        #return self._status
        pass

    @property
    def public_ips(self):
        return []

    @property
    def private_ips(self):
        return []

    @abstractmethod
    def destroy(self):
        """ Destroy this instance. """
        pass

    @abstractmethod
    def reboot(self):
        """ Reboot this instance. """
        pass

    @abstractmethod
    def start(self):
        """ Start this instance. """
        pass

    @abstractmethod
    def stop(self):
        """ Stop this instance. """
        pass

    def __repr__(self):
        return '<Instance: id={}, name={}, status={}, public_ips={}, private_ips={}>'.format(self.id, self.name, self.status, self.public_ips, self.private_ips)

    # BEGIN OF DEPRECATED INTERFACE

    @property
    def state(self):
        return self._status

    def stop_node(self):
        self.stop()

    # END OF DEPRECATED INTERFACE


@unique
class LocationKind(Enum):
    """ Represent the kind of a location. """

    GENERIC = auto()
    GLOBAL = auto()
    REGION = auto()
    ZONE = auto()


class Location:
    """ A physical location where cloud resources are hosted.  """

    def __init__(self, id, name=None, kind=LocationKind.GENERIC, parent=None, extra=None):
        self.id_ = id
        self._name = name if name is not None else id
        self._parent = parent
        self._kind = kind
        self._extra = extra

    @property
    def id(self):
        return self._id

    @property
    def name(self):
        return self._name

    @property
    def kind(self):
        return self._kind

    @property
    def parent(self):
        return self._parent

    @property
    def extra(self):
        return self._extra
