from abc import ABC, abstractmethod
from configparser import ConfigParser
import os


class MetaConfManager(ABC):
    """
    Common base class for all the configuration managers.
    """

    def __init__(self, platform, config_file=None):
        """
        Initializes parser object

        Args:
            platform (str): platform module directory name
            config_file (str): optional configuration file name.
        """
        self.platform = platform
        self.parser = ConfigParser()
        #self.parser.read("modules" + sep + self._platform + sep + "settings.cfg")
        if config_file:
            self._config_file = config_file
        else:
            self._config_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir, "modules", platform, "settings.cfg")

        # NOTE: this will raise an error if the configuration file does not exist
        self.parser.read_file(open(self._config_file, "r"))

        self.read_login_data()
        self.read_platform_options()
        self.read_options()

    @abstractmethod
    def read_login_data():
        pass

    @abstractmethod
    def read_platform_options():
        pass

    def read_options(self):
        """
        Reads options values from [options] section
        """
        self.monitor_fetch_period = self.get_parameter("options", "monitor_fetch_period", return_type=int)
        self.granularity = self.get_parameter("options", "granularity", return_type=int)
        self.window_size = self.get_parameter("options", "window_size", return_type=int)
        self.minimum_positive = self.get_parameter("options", "minimum_positive", return_type=int)

    def get_parameter(self, section, option, return_type=None, regex=None, default=None):
        """ Return the parameter stored in the module configuration file or
        the given default value.

        Args:
            section (str): name of the section (a name surrounded with square
                brackets inside the configuration file).
            option (str): name of the parameter.
            return_type (<return_type>): the type to which convert the parameter
                value read from the configuration file and returned from this
                function.
            regex (str): the regular expression the input must follow, if a user input is
                required.

        Returns:
            <return_type>: the parameter value converted to the given return type.
        """
        if self.parser.has_option(section, option):
            value = self.parser.get(section, option)
            if return_type == bool:
                if value in ["True", "true"]:
                    return True
            else:
                return return_type(value)
        else:
            return default

    def get_parameters(self, section):
        """Return the parameters stored in the given section of the module
        configuration file.

        Args:
            section (str): name of the section (a name surrounded with square
                brackets inside the configuration file).

        Returns:
            dict: A dictionary of options available in the given section.
        """
        items = dict()
        if self.parser.has_section(section):
            for item in self.parser.items(section):
                items[item[0]] = item[1]
        return items

    def has_section(self, section):
        """Tells whether the given section is present in the configuration.

        Args:
            section (str): name of the section (a name surrounded with square
                brackets inside the configuration file).

        Returns:
            bool: True if the given section exists, or False otherwise.
        """
        return self.parser.has_section(section)

    def has_parameter(self, section, param):
        """Tells whether the given parameter is present in the given section of
        the configuration.

        Args:
            section (str):  name of the section (a name surrounded with square brackets
                inside the configuration file).
            param (str): name of the parameter.

        Returns:
            bool: True if the given parameter exists, or False otherwise.
        """
        return self.parser.has_option(section, param)
