##
## Python module for storing measures to Pivotal RabbitMQ.
##
## Copyright 2020 Marco Guazzone (marco.guazzone@gmail.com)
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

from easycloud.core import measures_sink
import pika
import json
import logging


class RabbitMQSink(measures_sink.MeasuresSink):
    """Measures sink that interfaces with Pivotal RabbitMQ
       (https://rabbitmq.com/).

    Tested with Pivotal RabbitMQ v. 3.8.2 and Pika v. 1.1.0.

    Note that the exchange is not created automatically; it must already
    exist in RabbitMQ.

    References:
    - Pivotal RAbbitMQ: https://rabbitmq.com
    - Pika: https://github.com/pika/pika
    """

    DEFAULT_URL = 'amqp://localhost:5672'
    DEFAULT_EXCHANGE = 'easycloud'

    @classmethod
    def from_conf(cls, conf):
        url = None
        if 'url' in conf:
            url = conf['url'].strip()
        exchange = None
        if 'exchange' in conf:
            exchange = conf['exchange']
        return cls(url=url, exchange=exchange)

    def __init__(self, url=DEFAULT_URL, exchange=DEFAULT_EXCHANGE):
        super(RabbitMQSink, self).__init__()
        self._url = url if url is not None and len(url) > 0 else self.DEFAULT_URL
        self._exchange = exchange if exchange is not None and len(exchange) > 0 else self.DEFAULT_EXCHANGE
        # Created the exchange, if needed
        try:
            conn = pika.BlockingConnection(pika.URLParameters(self._url))
            channel = conn.channel()
            channel.exchange_declare(exchange=self._exchange, exchange_type='direct')
        except pika.exceptions.AMQPError as e:
            logging.warning('Problems while declaring the exchange: {}'.format(e))

    def put(self, measure):
        msg_key = self._measure_group_encode(self._measure_group(measure))
        msg = self._make_message(measure)
        try:
            conn = pika.BlockingConnection(pika.URLParameters(self._url))
            #credentials = pika.PlainCredentials('guest', 'guest')
            #parameters = pika.ConnectionParameters('localhost', credentials=credentials)
            #connection = pika.BlockingConnection(parameters)
            channel = conn.channel()
            props = pika.BasicProperties(content_type='application/json')
            channel.basic_publish(exchange=self._exchange, routing_key=msg_key, body=msg, properties=props)
            conn.close()
        except pika.exceptions.AMQPError as e:
            logging.warning('Unable to deliver message: {}'.format(e))

    def mput(self, measures):
        #try:
        #    conn = pika.BlockingConnection(pika.URLParameters(self._url))
        #    channel = conn.channel()
        #    props = pika.BasicProperties(content_type='application/json')
        #    for measure in measures:
        #        channel.basic_publish(exchange=self._exchange, routing_key=msg_key, body=msg, properties=props)
        #    conn.close()
        #except pika.exceptions.AMQPError as e:
        #    logging.warning('Unable to deliver message: {}'.format(e))
        # Groups measures by object namespace, object ID and metric
        measures_groups = dict() # {namespace => {object-id => {metric => [measure1, measure2, ...]}}}
        for measure in measures:
            group_id = self._measure_group_encode(self._measure_group(measure))
            if group_id not in measures_groups:
                measures_groups[group_id] = []
            measures_groups[group_id].append(measure)
        for group_id in measures_groups:
            msg_key = group_id
            for measure in measures_groups[group_id]:
                msg = self._make_message(measure)
                try:
                    conn = pika.BlockingConnection(pika.URLParameters(self._url))
                    channel = conn.channel()
                    props = pika.BasicProperties(content_type='application/json')
                    channel.basic_publish(exchange=self._exchange, routing_key=msg_key, body=msg, properties=props)
                    conn.close()
                except pika.exceptions.AMQPError as e:
                    logging.warning('Unable to deliver message: {}'.format(e))

    def _make_message(self, measure):
        return json.dumps({'timestamp': measure.timestamp.isoformat(),
                           'object-id': measure.object_id,
                           'object_ns': measure.object_ns,
                           'metric': measure.metric,
                           'unit': measure.unit,
                           'value': measure.value})
