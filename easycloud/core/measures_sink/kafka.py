##
## Python module for storing measures to Apache Kafka.
##
## Copyright 2020 Marco Guazzone (marco.guazzone@gmail.com)
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

#from confluent_kafka import KafkaException
from confluent_kafka import Producer
from easycloud.core import measures_sink
import json
import logging


class KafkaSink(measures_sink.MeasuresSink):
    """Measures sink that interfaces with Apache Kafka
       (https://kafka.apache.org/).

    Tested with Apache Kafka v. 2.4.0 and Confluent's Kafka Python client v. 1.3.0.

    References:
    - Apache Kafka: https://kafka.apache.org/
    - Confluent's Kafka Python client: https://github.com/confluentinc/confluent-kafka-python
    """

    DEFAULT_BROKERS = ['localhost:9092']
    DEFAULT_TOPIC = 'easycloud'

    @classmethod
    def from_conf(cls, conf):
        brokers = None
        if 'brokers' in conf:
            brokers = [ b.strip() for b in conf['brokers'].split(',') ]
        topic = None
        if 'topic' in conf:
            topic = conf['topic']
        return cls(brokers=brokers, topic=topic)

    def __init__(self, brokers=DEFAULT_BROKERS, topic=DEFAULT_TOPIC):
        super(KafkaSink, self).__init__()
        self._brokers = brokers if (brokers is not None) and len(brokers) > 0 else self.DEFAULT_BROKERS
        self._topic = topic if (topic is not None) and len(topic) > 0 else self.DEFAULT_TOPIC
        self._prod = Producer({'bootstrap.servers': ','.join(self._brokers)})

    def put(self, measure):
        msg_key = self._measure_group_encode(self._measure_group(measure))
        msg = self._make_message(measure)
        try:
            self._prod.produce(self._topic, key = msg_key, value = msg, callback=self._delivery_callback)

        except BufferError:
            logging.debug('Local producer queue is full ({} messages awaiting delivery): try again'.format(len(self._prod)))

        # Serve delivery callback queue.
        # NOTE: Since produce() is an asynchronous API this poll() call
        #       will most likely not serve the delivery callback for the
        #       last produce()d message.
        self._prod.poll(0)

        # Wait until all messages have been delivered
        logging.debug('Waiting for {} deliveries'.format(len(self._prod)))
        self._prod.flush()

    def mput(self, measures):
        # Groups measures by object namespace, object ID and metric to optimize writes on CSV files
        measures_groups = dict() # {namespace => {object-id => {metric => [measure1, measure2, ...]}}}
        for measure in measures:
            group_id = self._measure_group_encode(self._measure_group(measure))
            if group_id not in measures_groups:
                measures_groups[group_id] = []
            measures_groups[group_id].append(measure)
        for group_id in measures_groups:
            msg_key = group_id
            for measure in measures_groups[group_id]:
                msg = self._make_message(measure)
                try:
                    self._prod.produce(self._topic, key = msg_key, value = msg, callback=self._delivery_callback)
                #TODO: handle failures...
                #except KafkaException as e:
                #    logging.error("Produce message failed: {}".format(e))
                except BufferError:
                    logging.debug('Local producer queue is full ({} messages awaiting delivery): try again'.format(len(self._prod)))

                # Serve delivery callback queue.
                # NOTE: Since produce() is an asynchronous API this poll() call
                #       will most likely not serve the delivery callback for the
                #       last produce()d message.
                self._prod.poll(0)

        # Wait until all messages have been delivered
        logging.debug('Waiting for {} deliveries'.format(len(self._prod)))
        self._prod.flush()

    def _make_message(self, measure):
        return json.dumps({'timestamp': measure.timestamp.isoformat(),
                           'object-id': measure.object_id,
                           'object_ns': measure.object_ns,
                           'metric': measure.metric,
                           'unit': measure.unit,
                           'value': measure.value})

    # Optional per-message delivery callback (triggered by poll() or flush())
    # when a message has been successfully delivered or permanently
    # failed delivery (after retries).
    @staticmethod
    def _delivery_callback(err, msg):
        if err:
            logging.error('Message failed delivery: {}'.format(err))
        else:
            logging.debug('Message delivered to {} [{}] @ {}'.format(msg.topic(), msg.partition(), msg.offset()))
