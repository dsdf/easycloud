"""
Utility functions.
"""


def make_dict_proxy_wrapper(obj):
    """ Wraps the given object with a proxy class so as to access members
    of the wrapped object in a dictionary-like fashion.

    For instance, if ``obj`` has a property ``a``, then, after wrapping
    ``obj``, such property can accessed as ``obj['a']``.
    Note that inheriting the ``obj.__class__`` is needed to still use the
    wrapper object in the same way as the wrapped object.
    For instance, if ``obj`` has a method ``m``, we can still invoke such
    method on the related wrapper object.
    Likewise, if ``obj'' has a property ``a``, we can retrieve its value
    either by ``wrapped_obj.a`` or by ``wrapped_obj['a']``.
    """
    class Wrapper(obj.__class__):
        def __getitem__(self, key):
            return getattr(self, key)

        def __setitem__(self, key, value):
            setattr(self, key, value)
    return Wrapper()
