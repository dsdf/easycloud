from botocore.exceptions import WaiterError
from easycloud.core.metaconfmanager import MetaConfManager
import math


class AWSBaseConfManager(MetaConfManager):
    """
    Amazon Web Services configuration manager
    """

    def __init__(self, platform_name, config_file=None):
        """
        Init method (object initialization)
        """
        super().__init__(platform_name, config_file)

    def read_login_data(self):
        """
        Read login data from settings.cfg
        """
        self.ec2_access_key_id = self.get_parameter("aws", "ec2_access_key_id", return_type=str)
        self.ec2_secret_access_key = self.get_parameter("aws", "ec2_secret_access_key", return_type=str)
        self.ec2_session_token = self.get_parameter("aws", "ec2_session_token", return_type=str)
        self.ec2_default_region = self.get_parameter("aws", "ec2_default_region", return_type=str)

    def read_platform_options(self):
        """
        Read platform-specific options from settings.cfg
        """
        # Free Tier settings
        self.freetier_only = self.get_parameter("freetier", "freetier_only", return_type=bool)
        self.freetier_instance_types = self.parser.get(
            "freetier", "freetier_instance_types").replace(" ", "").replace("\n", "").split(",")
        self.freetier_images_ids = self.parser.get(
            "freetier", "freetier_images_ids").replace(" ", "").replace("\n", "").split(",")
        # Filters (for images, Amazon has 30.000+ images available)
        self.images_filters = self.parser.get(
            "filters", "images_filters").replace(" ", "").replace("\n", "").split(",")


class AWSEC2Utils:
    @staticmethod
    def wait_for_instance_operation(client, operation, instances, timeout=None):
        """ Wait for the given operation to complete.
        Args:
            client (boto.EC2.Client) the AWS EC2 client.
            operation (str): the operation to wait for (e.g., 'instance_running').
            instances (list): the list of instance IDs.
            timeout (float): an optional value representing the max number of
                seconds to wait for; if None, no timeout is enforced.

        See:
        - https://boto3.amazonaws.com/v1/documentation/api/latest/guide/clients.html
        - https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html#waiters
        """
        waiter = client.get_waiter(operation)
        if timeout is not None:
            delay = 5
            waiter_config = {'Delay': delay, 'MaxAttempts': math.ceil(timeout/delay)}
        ret = True
        check = True
        while check:
            try:
                if timeout is None:
                    waiter.wait(InstanceIds = instances)
                else:
                    waiter.wait(InstanceIds = instances, WaiterConfig = waiter_config)
                check = False
            except WaiterError:
                if timeout is not None:
                    check = False
                    ret = False
        return ret
