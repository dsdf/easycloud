from easycloud.core.metaconfmanager import MetaConfManager


class OpenStackBaseConfManager(MetaConfManager):
    """
    OpenStack base configuration manager.
    """

    def __init__(self, platform, config_file=None):
        """
        Init method (object initialization)
        """
        super().__init__(platform, config_file)

    def read_login_data(self):
        """
        Read login data from settings.cfg
        """
        # URL Regex took from http://www.noah.org/wiki/RegEx_Python#URL_regex_pattern
        self.os_auth_url = self.get_parameter(
            "openstack", "os_auth_url", return_type=str,
            regex="http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\\(\\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+"
        )
        self.os_username = self.get_parameter("openstack", "os_username", return_type=str)
        self.os_password = self.get_parameter("openstack", "os_password", return_type=str)
        self.os_project_name = self.get_parameter("openstack", "os_project_name", return_type=str)
        self.os_project_id = self.get_parameter("openstack", "os_project_id", return_type=str, default=None)
        self.os_region = self.get_parameter("openstack", "os_region", return_type=str)
        self.os_user_domain_name = self.get_parameter("openstack", "os_user_domain_name", return_type=str,
                                                      default="Default")
        self.os_project_domain_name = self.get_parameter("openstack", "os_project_domain_name", return_type=str,
                                                         default="Default")

        # OIDC Params
        self.identity_provider = self.get_parameter("openstack", "identity_provider", return_type=str)
        self.protocol = self.get_parameter("openstack", "protocol", return_type=str)
        self.discovery_endpoint = self.get_parameter("openstack", "discovery_endpoint", return_type=str)
        self.client_id = self.get_parameter("openstack", "client_id", return_type=str)
        self.client_secret = self.get_parameter("openstack", "client_secret", return_type=str, default=None)
        self.access_token_type = self.get_parameter("openstack", "access_token_type", return_type=str)

        # Telemetry
        self.os_telemetry_metering = self.get_parameter("openstack", "os_telemetry_metering", return_type=str,
                                                        default=None)

    def read_platform_options(self):
        self.demo_reservation_id = self.get_parameter("re_demo", "demo_reservation_id", return_type=str)
        """
        Read platform-specific options from settings.cfg
        """
        # Nothing to do here
        pass
