from easycloud.core.metaconfmanager import MetaConfManager
import time


class GCPBaseConfManager(MetaConfManager):
    """
    Google Cloud Platform base configuration manager.
    """

    SERVICE_ACCOUNT_AUTH_TYPE = 'service-account'
    OAUTH2_CLIENT_AUTH_TYPE = 'oauth2-client'

    def __init__(self, platform, config_file=None):
        """
        Init method (object initialization)
        """
        super().__init__(platform, config_file)

    def read_login_data(self):
        """
        Read login data from settings.cfg
        """
        self.gcp_auth_type = self.get_parameter("gcp", "gcp_auth_type", return_type=str)
        # Make sure to be backward compatible
        if self.gcp_auth_type is None or len(self.gcp_auth_type) == 0:
            self.gcp_auth_type = OAUTH2_CLIENT_AUTH_TYPE
        self.gcp_access_key_id = self.get_parameter("gcp", "gcp_access_key_id", return_type=str)
        self.gcp_secret_access_key = self.get_parameter("gcp", "gcp_secret_access_key", return_type=str)
        self.gcp_client_secrets_file = self.get_parameter("gcp", "gcp_client_secrets_file", return_type=str)
        self.gcp_service_account_file = self.get_parameter("gcp", "gcp_service_account_file", return_type=str)
        self.gcp_service_account_email = self.get_parameter("gcp", "gcp_service_account_email", return_type=str)
        self.gcp_zone = self.get_parameter("gcp", "gcp_zone", return_type=str)
        self.gcp_project = self.get_parameter("gcp", "gcp_project", return_type=str)
        # URL Regex took from http://www.noah.org/wiki/RegEx_Python#URL_regex_pattern
        '''
        self.gcp_auth_uri = self.get_parameter("gcp", "gcp_auth_uri",
                                               return_type=str,
                                               regex="http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\\(\\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+")
        self.gcp_token_uri = self.get_parameter("gcp", "gcp_token_uri",
                                                return_type=str,
                                                regex="http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\\(\\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+")
        self.gcp_redirect_uri = self.parser.get("gcp", "gcp_redirect_uri").replace(
            " ", "").replace("\n", "").split(",")
                '''

    def read_platform_options(self):
        """
        Read platform-specific options from settings.cfg
        """
        # Always Free settings
        self.alwaysfree_only = self.get_parameter("alwaysfree", "alwaysfree_only", return_type=bool)
        self.alwaysfree_instance_types = self.parser.get(
            "alwaysfree", "alwaysfree_instance_types").replace(" ", "").replace("\n", "").split(",")
        self.alwaysfree_zones = self.parser.get(
            "alwaysfree", "alwaysfree_zones").replace(" ", "").replace("\n", "").split(",")


class GCPComputeUtils:
    @staticmethod
    def wait_for_operation(service, project, operation, timeout=None):
        """ Wait until the given operation is done.
        Args:
            service (googleapiclient.discovery.Resource): the Resource object for interacting with the Google Compute service.
            project (str): the project identifier.
            operation (dict): the operation to wait for as returned by the
                original request.
            timeout (float): an optional value representing the max number of
                seconds to wait for; if None, no timeout is enforced.
        Returns:
            bool: True if the operation suceeded or False if the operation
                failed or if it does not complete successfully before the
                timeout.

        Requires one of the following OAuth scopes:
        - https://www.googleapis.com/auth/compute
        - https://www.googleapis.com/auth/cloud-platform
        and the followin IAM permissions:
        - compute.zoneOperations.get

        See:
        - https://cloud.google.com/compute/docs/reference/rest/v1/zoneOperations/get
        """
        # Inspired by: https://github.com/GoogleCloudPlatform/java-docs-samples/blob/master/compute/cmdline/src/main/java/ComputeEngineSample.java
        #FIXME: the following function has not been tested yet.
        op = operation['name']
        zone = None
        if 'zone' in operation and operation['zone']:
            zone = operation['zone'].rpartition('/')[2]
        start_time = time.time()
        pool_interval = 5
        ret = True
        try:
            while operation is not None and operation['status'] != 'DONE':
                time.sleep(poll_interval)
                if timeout is not None and (time.time()-start_time) >= timeout:
                    ret = False
                    break

                request = None
                if zone is None:
                    request = service.globalOperations().get(project=project,
                                                             operation=op)
                else:
                    request = service.zoneOperations().get(project=project,
                                                           zone=zone,
                                                           operation=op)
                operation = request.execute()

                if operation is not None and ('error' in operation or 'httpErrorMessage' in operation):
                    msg = ', '.join([ '{} (code: {})'.format(error['message'], error['code']) for error in operation['error']['errors'] ])
                    raise Exception(msg)
        except Exception as e:
            raise e
        return ret
