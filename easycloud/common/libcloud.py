from easycloud.core.compute import Instance, InstanceStatus


class LibcloudInstance(Instance):
    """ A libcloud instance. """

    def __init__(self, node):
        self._node = node
        self._status = InstanceStatus.from_string(node.state)

    @property
    def handle(self):
        return self._node

    @property
    def id(self):
        return self._node.id

    @property
    def name(self):
        return self._node.name

    @property
    def private_ips(self):
        return self._node.private_ips

    @property
    def public_ips(self):
        return self._node.public_ips

    @property
    def status(self):
        return self._status

    @property
    def extra(self):
        return self._node.extra

    def destroy(self):
        return self._node.destroy()

    def reboot(self):
        return self._node.reboot()

    def start(self):
        return self._node.start()

    def stop(self):
        return self._node.stop_node()
