"""
EasyCloud Google Cloud Platform Manager
"""


import datetime
#from easycloud.common.gcp import GCPComputeUtils
from easycloud.core.actionbinder import bind_action
from easycloud.core.compute import Instance, InstanceStatus, Location, LocationKind
from easycloud.core.metamanager import MetaManager
from easycloud.modules.gcp_googleapi.actions import GCPAgentActions
from easycloud.modules.gcp_googleapi.confmanager import GCPConfManager
from easycloud.modules.gcp_googleapi.monitor import GCPMonitor
from easycloud.tui.simpletui import SimpleTUI
#import google.oauth2.credentials
import googleapiclient.discovery
import google.auth
import google_auth_oauthlib.flow
import google.oauth2.service_account
#from google.auth import compute_engine
import pytz
import re
import time


class GCPRegion(Location):
    def __init__(self, gce_conn, gce_region):
        #self._gce_conn = gce_conn
        #self.gce_region_ = gce_region
        #extra = {}
        #extra['status'] = gce_region['status']
        #extra['zones'] = gce_region['zones']
        super().__init__(gce_region['id'],
                         name = gce_region['name'],
                         kind = LocaltionKind.REGION,
                         parent = None, # A region has no parent
                         extra = gce_region)


class GCPZone(Location):
    def __init__(self, gce_conn, gce_zone):
        #self._gce_conn = gce_conn
        #self.gce_zone_ = gce_zone
        #extra = {}
        #extra['status'] = gce_region['status']
        super().__init__(gce_zone['id'],
                         name = gce_zone['name'],
                         kind = LocaltionKind.ZONE,
                         #[XXX] Replaced by a more efficient solution
                         ## To get the parent region name use the zone's selfLink common to both zones and regions; for instance:
                         ##   "region": "https://www.googleapis.com/compute/v1/projects/easycloud-298013/regions/us-east1",
                         ##   "selfLink": "https://www.googleapis.com/compute/v1/projects/easycloud-298013/zones/us-east1-b",
                         ## The common part is "https://www.googleapis.com/compute/v1/projects/easycloud-298013/"
                         ##   region[(len(selfLink)- len("us-east1-b") - len("zones/") + len("regions/")):]
                         ##   == region[(len(selfLink) - len("us-east1-b") - 6 + 8):]
                         ##   == region[(len(selfLink) - len("us-east1-b") + 2):]
                         #parent = gce_zone['region'][(len(gce_zone['selfLink']) - len(gce_zone['name']) + 2):], 
                         #[/XXX]
                         parent = gce_zone['region'].rpartition('/')[2],
                         extra = gce_zone)


class GCPInstance(Instance):
    """ A GCP instance. """

    # Example instance:
    #   {'id': '8186249574343836981', 'creationTimestamp': '2020-12-15T05:36:27.126-08:00', 'name': 'easycloud-045', 'tags': {'fingerprint': '42WmSpB8rSM='}, 'machineType': 'https://www.googleapis.com/compute/v1/projects/easycloud-298013/zones/us-west1-a/machineTypes/e2-medium', 'status': 'TERMINATED', 'zone': 'https://www.googleapis.com/compute/v1/projects/easycloud-298013/zones/us-west1-a', 'canIpForward': False, 'networkInterfaces': [{'network': 'https://www.googleapis.com/compute/v1/projects/easycloud-298013/global/networks/default', 'subnetwork': 'https://www.googleapis.com/compute/v1/projects/easycloud-298013/regions/us-west1/subnetworks/default', 'networkIP': '10.138.0.22', 'name': 'nic0', 'fingerprint': 'bZTH_a0erZA=', 'kind': 'compute#networkInterface'}], 'disks': [{'type': 'PERSISTENT', 'mode': 'READ_WRITE', 'source': 'https://www.googleapis.com/compute/v1/projects/easycloud-298013/zones/us-west1-a/disks/easycloud-045', 'deviceName': 'easycloud-045', 'index': 0, 'boot': True, 'autoDelete': True, 'licenses': ['https://www.googleapis.com/compute/v1/projects/debian-cloud/global/licenses/debian-10-buster'], 'interface': 'SCSI', 'guestOsFeatures': [{'type': 'VIRTIO_SCSI_MULTIQUEUE'}, {'type': 'UEFI_COMPATIBLE'}], 'diskSizeGb': '10', 'kind': 'compute#attachedDisk'}], 'metadata': {'fingerprint': '9JH3OR_Y6Es=', 'kind': 'compute#metadata'}, 'serviceAccounts': [{'email': '596287920035-compute@developer.gserviceaccount.com', 'scopes': ['https://www.googleapis.com/auth/devstorage.read_only', 'https://www.googleapis.com/auth/logging.write', 'https://www.googleapis.com/auth/monitoring.write', 'https://www.googleapis.com/auth/service.management.readonly', 'https://www.googleapis.com/auth/servicecontrol', 'https://www.googleapis.com/auth/trace.append']}], 'selfLink': 'https://www.googleapis.com/compute/v1/projects/easycloud-298013/zones/us-west1-a/instances/easycloud-045', 'scheduling': {'onHostMaintenance': 'MIGRATE', 'automaticRestart': True, 'preemptible': False}, 'cpuPlatform': 'Unknown CPU Platform', 'labelFingerprint': '42WmSpB8rSM=', 'startRestricted': False, 'deletionProtection': False, 'reservationAffinity': {'consumeReservationType': 'ANY_RESERVATION'}, 'shieldedInstanceConfig': {'enableSecureBoot': False, 'enableVtpm': True, 'enableIntegrityMonitoring': True}, 'shieldedInstanceIntegrityPolicy': {'updateAutoLearnPolicy': True}, 'fingerprint': 'uFOs00PHE9Y=', 'lastStartTimestamp': '2020-12-15T05:36:33.656-08:00', 'lastStopTimestamp': '2020-12-15T05:41:54.622-08:00', 'kind': 'compute#instance'}
    #
    #See: https://cloud.google.com/compute/docs/reference/rest/v1/instances
    #     https://cloud.google.com/compute/docs/instances/instance-life-cycle
    #     https://github.com/google/python-cloud-utils/blob/master/cloud_utils/list_instances.py

    # See: https://cloud.google.com/compute/docs/instances/instance-life-cycle
    _NODE_STATUS_MAP = {
        "PROVISIONING": InstanceStatus.PENDING,
        "STAGING": InstanceStatus.PENDING,
        "RUNNING": InstanceStatus.RUNNING,
        "STOPPING": InstanceStatus.STOPPING,
        "REPAIRING": InstanceStatus.UNKNOWN,
        "TERMINATED": InstanceStatus.STOPPED,
        "SUSPENDING": InstanceStatus.PENDING,
        "SUSPENDED": InstanceStatus.SUSPENDED,
        "UNKNOWN": InstanceStatus.UNKNOWN
    }

    def __init__(self, gce_conn, gce_project, gce_instance):
        self._gce_conn = gce_conn
        self._gce_inst = gce_instance
        self._gce_proj = gce_project
        self._status = self._NODE_STATUS_MAP.get(gce_instance['status'], InstanceStatus.UNKNOWN)
        self._zone = gce_instance['zone'].rpartition('/')[2]
        private_ips = []
        public_ips = []
        if 'networkInterfaces' in gce_instance:
            for network_interface in gce_instance['networkInterfaces']:
                private_ips.append(network_interface['networkIP'])
                if 'accessConfigs' in network_interface:
                    for access_config in network_interface['accessConfigs']:
                        public_ips.append(access_config['natIP'])
        self._private_ips = private_ips
        self._public_ips = public_ips
        #extra = {}
        #extra['disks'] = gce_instance.get('disks', [])
        #extra['networkInterfaces'] = gce_instance.get('networkInterfaces', [])
        #extra['zone'] = None #TODO: Get info about the gce_instance['zone'] by using the zones().get() method (see: https://cloud.google.com/compute/docs/reference/rest/v1/zones/get)
        #if 'items' in gce_instance['tags']:
        #    extra['tags'] = gce_instance['tags']['items']
        #else:
        #    extra['tags'] = []
        #self._extra = gce_inst

    @property
    def extra(self):
        return self._gce_inst

    @property
    def handle(self):
        return self._gce_inst

    @property
    def id(self):
        return self._gce_inst['id']

    @property
    def name(self):
        return self._gce_inst['name']

    @property
    def private_ips(self):
        return self._private_ips

    @property
    def public_ips(self):
        return self._public_ips

    @property
    def status(self):
        return self._status

    def destroy(self):
        self._gce_conn.instances().destroy(project = self._gce_proj,
                                           zone = self._zone,
                                           instance = self._gce_inst['id']).execute()
        return True

    def reboot(self):
        self._gce_conn.instances().reset(project = self._gce_proj,
                                         zone = self._zone,
                                         instance = self._gce_inst['id']).execute()
        return True

    def start(self):
        self._gce_conn.instances().start(project = self._gce_proj,
                                         zone = self._zone,
                                         instance = self._gce_inst['id']).execute()
        #TODO: call "GCPComputeUtils.wait_for_operation(self._gce_conn, self._gce_proj, res)" to perform a synchronous version of this method (where "res" is the value returned by the "start()" method)
        return True

    def stop(self):
        self._gce_conn.instances().stop(project = self._gce_proj,
                                        zone = self._zone,
                                        instance = self._gce_inst['id']).execute()
        #TODO: call "GCPComputeUtils.wait_for_operation(self._gce_conn, self._gce_proj, res)" to perform a synchronous version of this method (where "res" is the value returned by the "start()" method)
        return True


class GCP(MetaManager):

    def __init__(self, config_file=None, rule_file=None):
        super().__init__(rule_file=rule_file)
        self.platform_name = "Google Cloud Platform"
        self.conf = GCPConfManager(config_file)
        self.cloned_instances = []
        # self.snapshots = None
        self.gcp_client = None
        self.connect()

    @property
    def handle(self):
        return self.gcp_client

    # =============================================================================================== #
    #                                Platform-specific client creation                                #
    # =============================================================================================== #

    def connect(self):
        """
        Connection to the endpoint specified in the configuration file
        """
        # Define the authorization scopes the user needs to approve before using Google Cloud with EasyCloud
        # A list of Authorization scopes can be found at https://developers.google.com/identity/protocols/googlescopes
        SCOPES = ["https://www.googleapis.com/auth/compute",  # View and manage Google Compute Engine resources
                  "https://www.googleapis.com/auth/devstorage.full_control",  # Manage your data and permissions in Google Cloud Storage
                  "https://www.googleapis.com/auth/monitoring"  # View and write monitoring data for all of your Google projects
                  ]
        # Trying connection to endpoint
        # For authentication, see:
        #      https://cloud.google.com/docs/authentication
        #      https://cloud.google.com/iap/docs/authentication-howto
        #      https://googleapis.dev/python/google-auth/latest/index.html
        #      https://googleapis.dev/python/google-auth/latest/user-guide.html
        # Deprecated way:
        #from oauth2client.client import GoogleCredentials
        #from google.oauth2.credentials import Credentials
        #from libcloud.common.google import GoogleOAuth2Credential
        #credentials = GoogleCredentials.get_application_default() #TODO!!!
        #oauth_client = GoogleOAuth2Credential(user_id=self.conf.gcp_access_key_id,
        #                                      key=self.conf.gcp_secret_access_key,
        #                                      credential_file=credential_file,
        #                                      scopes=scopes)
        #creds = Credentials(oauth_client.access_token)
        # New way:
        credentials = None
        if self.conf.gcp_auth_type == self.conf.SERVICE_ACCOUNT_AUTH_TYPE:
            credentials = google.oauth2.service_account.Credentials.from_service_account_file(self.conf.gcp_service_account_file, scopes=SCOPES)
        elif self.conf.gcp_auth_type == self.conf.OAUTH2_CLIENT_AUTH_TYPE:
            flow = None
            if self.conf.gcp_client_secrets_file:
                flow = google_auth_oauthlib.flow.InstalledAppFlow.from_client_secrets_file(self.conf.gcp_client_secrets_file, SCOPES)
            else:
                flow = google_auth_oauthlib.flow.InstalledAppFlow.from_client_config(
                            {'installed': {
                                'project_id': self.conf.gcp_project_id,
                                'client_id': self.conf.gcp_access_key_id,
                                'client_secret': self.conf.gcp_secret_access_key,
                                'auth_uri': 'https://accounts.google.com/o/oauth2/auth',
                                'token_uri': 'https://oauth2.googleapis.com/token',
                                'auth_provider_x509_cert_url': 'https://www.googleapis.com/oauth2/v1/certs',
                                'redirect_uris': ['urn:ietf:wg:oauth:2.0:oob','http://localhost']}},
                            SCOPES)
            #credentials = flow.run_local_server(open_browser=False)
            credentials = flow.run_console()
        else:
            raise Exception("Unknown authentication type")

        # NOTE: the argument 'cache_discovery=False' is needed to avoid the message:
        #   WARNING:googleapiclient.discovery_cache:file_cache is unavailable when using oauth2client >= 4.0.0 or google-auth
        #   ...
        #   ImportError: file_cache is unavailable when using oauth2client >= 4.0.0 or google-auth
        self.gcp_client = googleapiclient.discovery.build('compute', 'v1', credentials=credentials, cache_discovery=False)

    def get_region(self, region):
        request = self.gcp_client.regions().get(project=project, region=region)
        if request is not None:
            response = request.execute()
            return GCPRegion(self.gcp_client, response)
        return None

    def get_zone(self, zone):
        request = self.gcp_client.zones().get(project=project, zone=zone)
        if request is not None:
            response = request.execute()
            return GCPZone(self.gcp_client, response)
        return None

    def list_regions(self):
        """
        Returns a list of regions.

        Returns:
            list:    A list of regions as ``easycloud.core.compute.Location`` objects..

        Requires one of the following OAuth scopes:
        * https://www.googleapis.com/auth/compute
        * https://www.googleapis.com/auth/cloud-platform
        Also, authorization requires one or more of the following IAM permissions:
        * compute.zones.list
        See: https://cloud.google.com/compute/docs/reference/rest/v1/regions
        """
        regions = []
        project = self.conf.gcp_project
        request = self.gcp_client.regions().list(project=project)
        while request is not None:
            response = request.execute()
        
            if 'items' in response:
                for raw_region in response['items']:
                    regions.append(GCPRegion(self.gcp_client, raw_region))
            request = self.gcp_client.regions().list_next(previous_request=request, previous_response=response)
        return regions

    def list_zones(self):
        """
        Returns a list of zones.

        Returns:
            list:    A list of zones as ``easycloud.core.compute.Location`` objects..

        Requires one of the following OAuth scopes:
        * https://www.googleapis.com/auth/compute
        * https://www.googleapis.com/auth/cloud-platform
        Also, authorization requires one or more of the following IAM permissions:
        * compute.zones.list
        See: https://cloud.google.com/compute/docs/reference/rest/v1/zones
        """
        zones = []
        project = self.conf.gcp_project
        request = self.gcp_client.zones().list(project=project)
        while request is not None:
            response = request.execute()
        
            if 'items' in response:
                for raw_zone in response['items']:
                    zones.append(GCPZone(self.gcp_client, raw_zone))
            request = self.gcp_client.zones().list_next(previous_request=request, previous_response=response)
        return zones

    def list_instances(self):
        """ Returns a list of instances.

        Returns:
            list:    A list of ``easycloud.core.compute.Instance`` objects.

        Requires one of the following OAuth scopes:
        * https://www.googleapis.com/auth/compute
        * https://www.googleapis.com/auth/cloud-platform
        Also, authorization requires one or more of the following IAM permissions:
        * compute.instances.list
        See: https://cloud.google.com/compute/docs/reference/rest/v1/instances
        """
        instances = []
        project = self.conf.gcp_project
        #if self.conf.gcp_zone is not None and len(self.conf.gcp_zone) > 0:
        if self.conf.gcp_zone:
            # Get instances only related to the given zone
            instances_request = self.gcp_client.instances().list(project=project, zone=self.conf.gcp_zone)

            while instances_request is not None:
                instances_response = instances_request.execute()

                if 'items' in instances_response:
                    for raw_instance in instances_response['items']:
                        instances.append(GCPInstance(self.gcp_client, project, raw_instance))

                instances_request = self.gcp_client.instances().list_next(previous_request=instances_request, previous_response=instances_response)
        else:
            # Get instances from every zones
            instances_request = self.gcp_client.instances().aggregatedList(project=project)

            while instances_request is not None:
                instances_response = instances_request.execute()

                if 'items' in instances_response:
                    #NOTE: (2020-12-14)
                    #      There is an error in the Google Cloud doc page:
                    #           https://cloud.google.com/compute/docs/reference/rest/v1/instances/aggregatedList
                    #      as according to that page the key 'items' should map onto [{"scopeName": string, "instances": [ ... ]}]
                    #for instances_scoped_list in instances_response['items'].items():
                    for zone, instances_scoped_list in instances_response['items'].items():
                        if 'instances' in instances_scoped_list:
                            for raw_instance in instances_scoped_list['instances']:
                                instance = GCPInstance(self.gcp_client, project, raw_instance)
                                instances.append(instance)

                instances_request = self.gcp_client.instances().aggregatedList_next(previous_request=instances_request, previous_response=instances_response)

        return instances

    # =============================================================================================== #
    #                                  Platform-specific list printers                                #
    # =============================================================================================== #

    def print_all_nics(self):
        """
        Print instance id, image id, IP address and state for each active instance
        ! NOTE: before using this method, please set an instance using self.instance = <instance> !

        Returns:
            int: the number of items printed
        """
        table_header = ["ID", "NIC Name",""]
        table_body = self._list_all_nics()
        SimpleTUI.print_table(table_header, table_body)
        if len(self.instances) == 0:
            SimpleTUI.info("There are no NICs available for this instance")
        return len(self.instances)

    def print_global_instances(self):
        """
        Print instance id, image id, IP address and state for each active instance in each region
        """
        table_header = ["ID", "Instance Name", "Instance ID",
                        "IP address", "Status", "Key Name", "Avail. Zone"]
        table_body = self._list_global_instances()
        SimpleTUI.print_table(table_header, table_body)
        if len(self.global_instances) == 0:
            SimpleTUI.info("There are no running or pending instances")
        return len(self.global_instances)

    def print_all_availability_zones(self):
        """
        Print all availability zones
        """
        table_header = ["ID", "Name", "Status", "Maintenance windows"]
        table_body = self._platform_list_all_availability_zones()
        SimpleTUI.print_table(table_header, table_body)
        if len(self.avail_zones) == 0:
            SimpleTUI.info("There are no availability zones available")
        return len(self.avail_zones)

    def print_global_volumes(self):
        """
        Print all the available volumes for each region and some informations
        """
        table_header = ["ID", "Volume Name", "Volume ID",
                        "Creation", "Size (GB)", "Attached To", "Status", "Avail. Zone"]
        table_body = self._list_global_volumes()
        SimpleTUI.print_table(table_header, table_body)
        if len(self.global_volumes) == 0:
            SimpleTUI.info("There are no volumes available")
        return len(self.global_volumes)

    def print_global_floating_ips(self):
        """
        Print ip and other useful information of all floating ip available in each region
        """
        table_header = ["ID", "Public Ip",
                        "Floating IP ID", "Associated Instance", "Region"]
        table_body = self._list_global_floating_ips()
        SimpleTUI.print_table(table_header, table_body)
        if len(self.global_floating_ips) == 0:
            SimpleTUI.info("There are no floating IPs available")
        return len(self.global_floating_ips)

    # =============================================================================================== #
    #                                         List builders                                           #
    # =============================================================================================== #

    def _platform_list_all_images(self):
        """
        Print all available images
        Format: "ID", "Name", Image ID", "State"
        """
        self.images = self.gcp_client.list_images()
        i = 1
        table_body = []
        for image in self.images:
            table_body.append([i, image.name, image.id, image.extra["status"]])
            i = i + 1
        return table_body

    def _platform_list_all_availability_zones(self):
        """
        Print all availability zones
        Format: "ID", "Name", "Status", "Maintenance windows"
        """
        self.avail_zones = self.gcp_client.ex_list_zones()
        i = 1
        table_body = []
        for avail_zone in self.avail_zones:
            table_body.append([i, avail_zone.name, avail_zone.status, avail_zone.maintenance_windows])
            i = i + 1
        return table_body

    def _platform_list_all_instance_types(self):
        """
        Print all instance types
        Format: "ID", "Instance Type ID", "vCPUs", "Ram (GB)", "Disk (GB)"
        """
        self.instance_types = self.gcp_client.list_sizes()
        if self.conf.alwaysfree_only:
            filtered_instance_types = []
            for instance_type in self.instance_types:
                if instance_type.name in self.conf.alwaysfree_instance_types:
                    filtered_instance_types.append(instance_type)
            self.instance_types = filtered_instance_types
        i = 1
        table_body = []
        for instance_type in self.instance_types:
            table_body.append([i, instance_type.name, instance_type.extra["guestCpus"],
                               instance_type.ram, instance_type.disk])
            i = i + 1
        return table_body

    def _platform_list_all_security_groups(self):
        """
        Print all security groups
        Format: "ID", "SG name", "SG description"
        """
        # Security groups not available on GCP
        # Please look for the firewalls
        pass

    def _platform_list_all_networks(self):
        """
        # Not used #
        Print id and other useful informations of all networks available
        Format: "ID", "Network name", "Description"
        """
        self.networks = self.gcp_client.ex_list_networks()
        i = 1
        table_body = []
        for network in self.networks:
            table_body.append([i, network.name, network.id])
            i = i + 1
        return table_body

    def _platform_list_all_firewalls(self):
        """
        Print informations about all the available firewalls
        Format: "ID", "Firewall name"
        """
        self.firewalls = self.gcp_client.ex_list_firewalls()
        i = 1
        table_body = []
        for firewall in self.firewalls:
            table_body.append([i, firewall.name])
            i = i + 1
        return table_body

    def _platform_list_all_key_pairs(self):
        """
        # Key pairs not available on GCP #

        Print all key pairs
        Format: "ID", "Key name", "Key fingerprint"
        """
        pass

    def _platform_list_all_instances(self):
        """
        Print instance id, image id, IP address and state for each active instance
        Format: "ID", "Instance Name", "Instance ID", "IP address", "Status", "Key Name", "Avail. Zone"
        """
        self.instances = self.list_instances()
        i = 1
        table_body = []
        for instance in self.instances:
            if len(instance.public_ips) > 0 and None not in instance.public_ips:
                table_body.append([i, instance.name, instance.id, ", ".join(instance.public_ips), instance.state, "n/a", instance.extra["zone"]])
            else:
                table_body.append([i, instance.name, instance.id, "-", instance.state, "n/a", instance.extra["zone"]])
            i = i + 1
        return table_body

    def _platform_list_all_volumes(self):
        """
        Print volumes alongside informations regarding attachments
        Format: "Volume ID", "Creation", "Size (GB)", "Attached To", "Status", "Avail. Zone"
        """
        self.volumes = self.gcp_client.list_volumes()
        i = 1
        table_body = []
        for volume in self.volumes:
            created_at_timestamp = volume.extra["creationTimestamp"]
            # Fix for Python 3.x (< 3.7), strptime doesn't recognize the colons of timezone, so they must be removed
            # See https://stackoverflow.com/questions/54268458/datetime-strptime-issue-with-a-timezone-offset-with-colons
            timestamp_tz_fix = re.sub(r'([-+]\d{2}):(\d{2})(?:(\d{2}))?$', r'\1\2\3', created_at_timestamp)
            created_at_datetime = datetime.datetime.strptime(timestamp_tz_fix, "%Y-%m-%dT%H:%M:%S.%f%z")
            created_at = created_at_datetime.astimezone(pytz.utc).strftime("%b %d %Y, %H:%M:%S") + " UTC"
            instance = self._get_instance_from_volume(volume)
            if instance is not None:
                table_body.append([i, volume.name, volume.id, created_at, volume.size,
                                   instance.name, volume.extra["status"], volume.extra["zone"].name])
            else:
                table_body.append([i, volume.name, volume.id, created_at, volume.size,
                                   "-", volume.extra["status"], volume.extra["zone"].name])
            i = i + 1
        return table_body

    def _platform_list_all_floating_ips(self):
        """
        Print ip and other useful information of all floating ip available
        Format: "ID", "Public Ip", "Floating IP ID", "Associated Instance", "Region"
        """
        self.floating_ips = self.gcp_client.ex_list_addresses()
        i = 1
        table_body = []
        for floating_ip in self.floating_ips:
            instance = self._get_instance_from_floating_ip(floating_ip)
            if instance is not None:
                table_body.append([i, floating_ip.address, floating_ip.id, instance.name, floating_ip.region.name])
            else:
                table_body.append([i, floating_ip.address, floating_ip.id, "-", floating_ip.region])
            i = i + 1
        return table_body

    # =============================================================================================== #
    #                                Platform-specific list builders                                  #
    # =============================================================================================== #

    def _list_all_nics(self):
        """
        Print id and other useful informations of all network interfaces
        available for a certain instance
        Format: "ID", "Network name", "IP Address"
        """
        i = 1
        table_body = []
        for network_interface in self.current_instance.extra["networkInterfaces"]:
            table_body.append([i, network_interface["name"], network_interface["networkIP"]])
            i = i + 1
        return table_body

    def _list_all_assigned_ips(self):
        self.instances = self.gcp_client.list_nodes()
        self.assigned_ips = []
        i = 1
        table_body = []
        for instance in self.instances:
            if len(instance.public_ips) > 0 and None not in instance.public_ips:
                self.assigned_ips.append(instance.public_ips[0])
                table_body.append([i, instance.public_ips[0], instance.name])
            i = i + 1
        return table_body

    def _list_global_instances(self):
        """
        Print instance id, image id, IP address and state for all the instances in all the zones
        Format: "ID", "Instance Name", "Instance ID", "IP address", "Status", "Key Name", "Avail. Zone"
        """
        self.global_instances = []
        zones = self.gcp_client.ex_list_zones()
        for zone in zones:
            self.global_instances += self.gcp_client.list_nodes(ex_zone=zone)
        i = 1
        table_body = []
        for instance in self.global_instances:
            if len(instance.public_ips) > 0 and None not in instance.public_ips:
                table_body.append([i, instance.name, instance.id, ", ".join(instance.public_ips), instance.state, "n/a", instance.extra["zone"]])
            else:
                table_body.append([i, instance.name, instance.id, "-", instance.state, "n/a", instance.extra["zone"]])
            i = i + 1
        return table_body

    def _list_global_volumes(self):
        """
        Print volumes alongside informations regarding attachments for all the volumes in all the zones
        Format: "Volume ID", "Creation", "Size (GB)", "Attached To", "Status", "Avail. Zone"
        """
        self.global_volumes = []
        zones = self.gcp_client.ex_list_zones()
        for zone in zones:
            self.global_volumes += self.gcp_client.list_volumes(ex_zone=zone)
        i = 1
        table_body = []
        for volume in self.global_volumes:
            created_at_timestamp = volume.extra["creationTimestamp"]
            # Fix for Python 3.x (< 3.7), strptime doesn't recognize the colons of timezone, so they must be removed
            # See https://stackoverflow.com/questions/54268458/datetime-strptime-issue-with-a-timezone-offset-with-colons
            timestamp_tz_fix = re.sub(r'([-+]\d{2}):(\d{2})(?:(\d{2}))?$', r'\1\2\3', created_at_timestamp)
            created_at_datetime = datetime.datetime.strptime(timestamp_tz_fix, "%Y-%m-%dT%H:%M:%S.%f%z")
            created_at = created_at_datetime.astimezone(pytz.utc).strftime("%b %d %Y, %H:%M:%S") + " UTC"
            instance = self._get_instance_from_volume(volume)
            if instance is not None:
                table_body.append([i, volume.name, volume.id, created_at, volume.size,
                                   instance.name, volume.extra["status"], volume.extra["zone"].name])
            else:
                table_body.append([i, volume.name, volume.id, created_at, volume.size,
                                   "-", volume.extra["status"], volume.extra["zone"].name])
            i = i + 1
        return table_body

    def _list_global_floating_ips(self):
        """
        Print ip and other useful information of all floating ip available
        Format: "ID", "Public Ip", "Floating IP ID", "Associated Instance", "Region"
        """
        self.global_floating_ips = self.gcp_client.ex_list_addresses(region="all")
        i = 1
        table_body = []
        for floating_ip in self.global_floating_ips:
            instance = self._get_instance_from_floating_ip(floating_ip)
            if instance is not None:
                table_body.append([i, floating_ip.address, floating_ip.id, instance.name, floating_ip.region.name])
            else:
                table_body.append([i, floating_ip.address, floating_ip.id, "-", floating_ip.region.name])
            i = i + 1
        return table_body

    # =============================================================================================== #
    #                                       Actions and Menus                                         #
    # =============================================================================================== #

    def _platform_get_region(self):
        """
        Get current region name

        Returns:
            str: the name of the current region
        """
        return self.conf.gcp_zone

    def _platform_create_new_instance(self, instance_name, image, instance_type, monitor_cmd_queue=None):
        """
        Create a new instance using the Google Cloud Platform API

        Args:
            instance_name (str): The name of the instance
            image (<image_type>): The image to be used as a base system
            instance_type (<instance_type>): The VM flavor
            monitor_cmd_queue: the monitor commands Quue
        """

        # Creation summary
        print("\n--- Creating a new instance with the following properties:")
        print("- %-20s %-30s" % ("Name", instance_name))
        print("- %-20s %-30s" % ("Image", image.name))
        print("- %-20s %-30s" % ("Instance Type", instance_type.name))

        # ask for confirm
        print("")
        if(SimpleTUI.user_yn("Are you sure?")):
            instance = self.gcp_client.create_node(name=instance_name,
                                                   size=instance_type,
                                                   image=image)
            if instance is None:
                return False
            if monitor_cmd_queue is not None and self.is_monitor_running():
                monitor_cmd_queue.put({"command": "add", "instance_id": instance.id})
            return True

    def _platform_instance_action(self, instance, action):
        """
        Handle an instance action with Google Cloud Platform API
        """
        if action == "reboot":
            return instance.reboot()
        elif action == "delete":
            return instance.destroy()

    def _platform_get_instance_info(self):
        """
        Return a list of instances info
        """
        info = []
        for instance in self.gcp_client.get_only_instances():
            info.append({"id": instance.id, "name": instance.name})
        return info

    def _platform_is_volume_attached(self, volume):
        """
        Check if a volume is attached to an instance

        Args:
            volume (<volume>): The volume to check

        Returns:
            bool: True if the volume is successfully attached, False otherwise
        """
        for instance in self.gcp_client.list_nodes():
            disks = instance.extra["disks"]
            if len(disks) > 0:
                for disk in disks:
                    if volume.name == disk["deviceName"]:
                        return True
        return False

    def _platform_detach_volume(self, volume):
        """
        Detach a volume using the Google Cloud Platform API
        Some specific steps are performed here:
            - Check if the selected volume is a boot disk

        Args:
            volume (<volume>): The volume to detach

        Returns:
            bool: True if the volume is successfully detached, False otherwise
        """
        # Search for the instance the volume is attached to
        instance = self._get_instance_from_volume(volume)
        if instance is None:
            return False
        if not self._is_volume_removable(volume):
            SimpleTUI.msg_dialog("Detaching status",
                                 "This volume is mounted as boot disk and cannot be detached until the VM is stopped!",
                                 SimpleTUI.DIALOG_ERROR)
            return
        result = self.gcp_client.detach_volume(volume, instance)
        if result:
            while True:
                # No direct way to refresh a Volume status, so we look if
                # it is still attached to its previous instance
                updated_volume = self.gcp_client.ex_get_volume(volume.name)
                if not self._platform_is_volume_attached(updated_volume):
                    break
                time.sleep(3)
        return result

    def _platform_delete_volume(self, volume):
        """
        Delete a volume using the Google Cloud Platform API
        Some specific steps are performed here:
            - Check if the selected volume is a boot disk

        Args:
            volume (<volume>): The volume to delete

        Returns:
            bool: True if the volume is successfully deleted, False otherwise
        """
        if not self._is_volume_removable(volume):
            SimpleTUI.msg_dialog("Volume deletion",
                                 "This volume is mounted as boot disk and cannot be detached until the VM is stopped!",
                                 SimpleTUI.DIALOG_ERROR)
            return
        return self.gcp_client.destroy_volume(volume)

    def _platform_attach_volume(self, volume, instance):
        """
        Attach a volume using the Google Cloud Platform API
        Some specific steps are performed here:
            - Device name (the name assigned to this device visible through the VM OS)
            - Mount mode (read only or read write)
            - Disk interface (SCSI or NVME)

        Args:
            volume (<volume>): The volume to attach
            instance (<instance>): The instance where the volume is to be attached

        Returns:
            bool: True if the volume is attached successfully, False otherwise
        """
        # Ask for mount ro/rw
        ro_rw = SimpleTUI.input_dialog("Mount mode",
                                       question="Specify a mount mode (READ_WRITE, READ_ONLY)",
                                       return_type=str,
                                       regex="^(READ_WRITE|READ_ONLY)$")
        if ro_rw is None:
            return
        # Ask for disk interface
        interface = SimpleTUI.input_dialog("Disk interface",
                                           question="Specify disk interface (SCSI, NVME)",
                                           return_type=str,
                                           regex="^(SCSI|NVME)$")
        return self.gcp_client.attach_volume(node=instance, volume=volume, device=volume.name,
                                             ex_boot=False, ex_auto_delete=False, ex_interface=interface)

    def _platform_create_volume(self, volume_name, volume_size):
        """
        Create a new volume using the Google Cloud Platform API
        Some specific steps are performed here:
            - Volume location (availability zone)
            - Volume type (pd-standard or pd-ssd)

        Args:
            volume_name (str): Volume name
            volume_size (int): Volume size in GB

        Returns:
            bool: True if the volume is successfully created, False otherwise
        """
        volume_location = self.conf.gcp_zone
        # if gcp_zone = none, let the user choose the location
        # in which to create the volume
        if self.conf.gcp_zone == "none":
            volume_index = SimpleTUI.list_dialog("Availability zones",
                                                 self.print_all_availability_zones,
                                                 question="Choose the availability zone for the volume")
            volume_location = self.avail_zones[volume_index - 1]
        # Volume type
        volume_type = SimpleTUI.input_dialog("Volume type",
                                             question="Specify the volume type (pd-standard, pd-ssd)",
                                             return_type=str,
                                             regex="^(pd-standard|pd-ssd)$")
        if volume_type is None:
            return
        # Volume creation
        return self.gcp_client.create_volume(size=volume_size, name=volume_name,
                                             ex_disk_type=volume_type, 
                                             location = volume_location)

    def _platform_is_ip_assigned(self, floating_ip):
        """
        # Not supported on GCP #
        Check if a floating IP is assigned to an instance

        Args:
            floating_ip (GCEAddress): The floating IP to check

        Returns:
            bool: True if the floating IP is assigned, False otherwise
        """
        return False

    def _platform_detach_floating_ip(self, floating_ip):
        # Not supported on GCP #
        """
        Detach a floating IP using the Google Cloud Platform API

        Args:
            floating_ip (GCEAddress): The floating IP to detach

        Returns:
            bool: True if the floating IP is successfully detached, False otherwise
        """
        pass

    def _platform_release_floating_ip(self, floating_ip):
        """
        Release a floating IP using the Google Cloud Platform API

        Args:
            floating_ip (GCEAddress): The floating IP to release

        Returns:
            bool: True if the floating IP is successfully released, False otherwise
        """
        return self.gcp_client.ex_destroy_address(floating_ip)

    def _platform_associate_floating_ip(self, floating_ip, instance):
        """
        Associate a floating IP to an instance using the Google Cloud Platform API
        Some specific steps are performed here:
            - NIC (Network interface controller) selection
            - Access config name

        Args:
            floating_ip (GCEAddress): The floating IP to attach
            instance (Node): The instance where the floating IP is to be assigned

        Returns:
            bool: True if the floating IP is successfully associated, False otherwise
        """
        
        # Set an instance, as required by print_all_nics()
        self.current_instance = instance
        nic_index = SimpleTUI.list_dialog("NICs available",
                                          self.print_all_nics,
                                          question="Select the VM NIC to assign this IP")
        if nic_index is None:
            return
        nic = instance.extra["networkInterfaces"][nic_index - 1]  # serve nome per rimuovere
        # Check if there's already an active Access Configuration and ask the user for confirm
        remove_old_access_config = False
        if self._nic_has_access_config(nic):
            choice = SimpleTUI.yn_dialog("Access Configuration Overwrite",
                                         "Warning: there's already an access configuration associated to this NIC.\n" +
                                         "Do you really want to continue (the current access configuration will be overwritten)?",
                                         warning=True)
            if not choice:
                return
            remove_old_access_config = True
        # Access Configuration name
        access_config_name = SimpleTUI.input_dialog("Access configuration",
                                                    question="Specify an access configuration name",
                                                    return_type=str,
                                                    regex="^[a-zA-Z0-9-]+$")
        if access_config_name is None:
            return
        # Remove the old access configuration if it's already existing
        if remove_old_access_config:
            SimpleTUI.msg_dialog("Access Configuration Overwrite", "Removing old access configuration...",
                                 SimpleTUI.DIALOG_INFO, pause_on_exit=False, clear_on_exit=False)
            if not self._delete_access_config(instance, nic):
                SimpleTUI.msg_dialog("Access Configuration Overwrite",
                                     "There was an error while removing the current access configuration!",
                                     SimpleTUI.DIALOG_ERROR)
                return
        # Associate the Access Configuration to the NIC
        if self.gcp_client.ex_add_access_config(node=instance, name=access_config_name,
                                                nic=nic, nat_ip=floating_ip.address,
                                                config_type="ONE_TO_ONE_NAT"):
            return True
        return False

    def _platform_reserve_floating_ip(self):
        """
        Reserve a floating IP using the Google Cloud Platform API
        """
        address_name = SimpleTUI.input_dialog("Floating IP Name",
                                              question="Specify a name for the new Floating IP",
                                              return_type=str,
                                              regex="^[a-zA-Z0-9-]+$")
        if address_name is None:
            return
        if self.gcp_client.ex_create_address(name=address_name, region='global'):
            return True
        return False

    def _platform_get_monitor(self, commands_queue, measurements_queue):
        """
        Create the Google Cloud Platform Monitor using StackDriver APIs

        Args:
            commands_queue (Queue): message queue for communicating with the main
                                    thread and receiving commands regarding the metrics
                                    to observe
            measurements_queue (Queue): message queue for sending measurements to
                                        the platform RuleEngine

        Returns:
            MetaMonitor: the platform-specific monitor
        """
        self.instances = self.gcp_client.list_nodes()
        for instance in self.instances:
            commands_queue.put({"command": "add", "instance_id": instance.name})
        return GCPMonitor(conf=self.conf,
                          commands_queue=commands_queue,
                          measurements_queue=measurements_queue)

    # =============================================================================================== #
    #                               Platform-specific Actions and Menus                               #
    # =============================================================================================== #

    def _nic_has_access_config(self, nic):
        """
        Check if an access configuration is assigned to a NIC

        Args:
            nic (NetworkInterface): The NIC to check

        Returns:
            bool: True if an access_configuration is already assigned
                  to this NIC, False otherwise
        """
        if len(nic["accessConfigs"]) > 0:
            return True
        return False

    def _is_volume_removable(self, volume):
        """
        Check if the volume can be detached from an instance or deleted

        Args:
            volume (StorageVolume): The volume to check
        """
        for instance in self.gcp_client.list_nodes(ex_zone=volume.extra["zone"]):
            disks = instance.extra["disks"]
            if len(disks) > 0:
                for disk in disks:
                    if volume.name == disk["deviceName"] and disk["boot"] and instance.state != "stopped":
                        return False
        return True

    def _delete_access_config(self, instance, nic):
        """
        Delete an access config associated to a network interface of a VM

        Args:
            instance (Node): The instanceto which the access config is
                             associated
            access_config_name (str): The access configuration name
            nic (str): The network interface controller name
        """
        return self.gcp_client.ex_delete_access_config(node=instance,
                                                       name=nic["accessConfigs"][0]["name"],
                                                       nic=nic["name"])

    def _get_instance_from_volume(self, volume):
        """
        Return the instance to which the volume is attached

        Args:
            volume (StorageVolume): the volume attached to an instance

        Returns:
            Node: an instance, or None if no instances are found
        """
        for instance in self.gcp_client.list_nodes(ex_zone=volume.extra["zone"]):
            disks = instance.extra["disks"]
            if len(disks) > 0:
                for disk in disks:
                    if volume.name == disk["deviceName"]:
                        return instance
        return None

    def _get_instance_from_floating_ip(self, floating_ip):
        """
        Return the instance to which the floating IP is attached

        Args:
            floating_ip (GCEAddress): the floating IP attached to an instance

        Returns:
            Node: an instance, or None if no instances are found
        """
        for instance in self.gcp_client.list_nodes(ex_zone="all"):
            if len(instance.public_ips) > 0 and None not in instance.public_ips:
                if instance.public_ips[0] == floating_ip.address:
                    return instance
        return None

    def _is_instance_floating_ip_static(self, instance):
        """
        Check if the floating IP is promoted to static

        Args:
            floating_ip (Node): The instance to check

        Returns:
            bool: True if the attached floating IP is static, false otherwise
        """
        if len(instance.public_ips) == 0 or None in instance.public_ips:
            return False
        self.floating_ips = self.gcp_client.ex_list_addresses()
        for floating_ip in self.floating_ips:
            if instance.public_ips[0] == floating_ip.address:
                return True
        return False

    def promote_ephimeral_ip(self):
        """
        Promote an Ephimeral IP to a Static one
        For more infos about Ephimeral/Static IPs on GCP, please visit
        https://cloud.google.com/compute/docs/ip-addresses/
        """
        # Select an instance
        floating_ip = None
        while(True):
            instance_index = SimpleTUI.list_dialog("Instances available",
                                                   self.print_all_instances,
                                                   question="Select the instance which floating IP has to be promoted to \"static\"")
            if instance_index is None:
                return
            instance = self.instances[instance_index - 1]
            # Check if the instance has an IP assigned (e.g. no IP is assigned while stopped)
            if len(instance.public_ips) == 0 or None in instance.public_ips:
                SimpleTUI.msg_dialog("Promotion status", "This instance has no available floating IPs to promote!",
                                     SimpleTUI.DIALOG_ERROR)
            # Check if the instance has already a static IP assigned
            elif self._is_instance_floating_ip_static(instance):
                SimpleTUI.msg_dialog("Promotion status", "This instance floating IP is already promoted to \"static\"!",
                                     SimpleTUI.DIALOG_ERROR)
            # Continue the ephimeral to static conversion
            else:
                floating_ip = instance.public_ips[0]
                break
        # Specify address name
        address_name = SimpleTUI.input_dialog("Static Floating IP Name",
                                              question="Specify a name for the new Static Floating IP",
                                              return_type=str,
                                              regex="^[a-zA-Z0-9-]+$")
        if address_name is None:
            return
        if self._promote_ephimeral_ip_to_static(floating_ip, address_name):
            SimpleTUI.msg_dialog("Static Floating IP Promotion", "Floating IP promoted!", SimpleTUI.DIALOG_SUCCESS)
        else:
            SimpleTUI.msg_dialog("Static Floating IP Promotion", "There was an error while promoting this Floating IP!", SimpleTUI.DIALOG_ERROR)

    def _promote_ephimeral_ip_to_static(self, floating_ip, address_name):
        """
        Create a new static ip starting from an ephimeral one

        Args:
            floating_ip (str): the IP to promote
            address_name (str): the symbolic name to assign to the new static IP

        Returns:
            bool: True if the creation is a success, False otherwise
        """
        return self.gcp_client.ex_create_address(name=address_name, address=floating_ip)

    def _platform_extra_menu(self):
        """
        Print the extra Functions Menu (specific for each platform)
        """
        while(True):
            menu_header = self.platform_name + " Extra Commands"
            menu_subheader = ["Region: \033[1;94m" +
                              self._platform_get_region() + "\033[0m"]
            menu_items = ["Promote ephimeral IP to static",
                          "Demote a static IP",
                          "List instances for all the regions",
                          "List volumes for all the regions",
                          "List floating ips for all the regions",
                          "Back to the Main Menu"]
            choice = SimpleTUI.print_menu(menu_header, menu_items, menu_subheader)
            if choice == 1:  # Promote IP
                self.promote_ephimeral_ip()
            elif choice == 2:  # Demote IP
                answer = SimpleTUI.yn_dialog("Demotion status",
                                             "You can demote a static IP easily deleting it through \"Manage floating IPs\" > \"Release a reserved Floating IP\".\n" +
                                             "NOTE: the static IP won't be removed from the associated instance until the latter is stopped/rebooted/deleted.\n" +
                                             "For more infos about Ephimeral/Static IPs on GCP, please visit https://cloud.google.com/compute/docs/ip-addresses/.\n" +
                                             "Would you like to start the \"Release a reserved Floating IP\" wizard now?", SimpleTUI.DIALOG_INFO)
                if answer:
                    self.release_floating_ip()
            elif choice == 3:  # List all the instances (Global)
                SimpleTUI.list_dialog("Instances available (Global view)",
                                      list_printer=self.print_global_instances)
            elif choice == 4:  # List all the volumes (Global)
                SimpleTUI.list_dialog("Volumes available (Global view)",
                                      list_printer=self.print_global_volumes)
            elif choice == 5:  # List all the floating ips (Global)
                SimpleTUI.list_dialog("Floating IPs available (Global view)",
                                      list_printer=self.print_global_floating_ips)
            elif choice == 6:
                break
            else:
                SimpleTUI.msg_dialog("Error", "Unimplemented functionality", SimpleTUI.DIALOG_ERROR)

    # =============================================================================================== #
    #                                         Override methods                                        #
    # =============================================================================================== #

    override_main_menu = []
    override_ips_menu = [5]
    override_volumes_menu = []

    def _platform_is_overridden(self, menu, choice):
        """
        Check if a menu voice is overridden by this platform

        Args:
            menu (str): a menu identifier
            choice (int): a menu entry index

        Returns:
            bool: the override status (True/False)
        """
        if menu == "floating_ips" and choice in self.override_ips_menu:
            if choice == 5:  # Volumes on barebone are not supported
                return True
        return False

    def _platform_override_menu(self, menu, choice):
        """
        Override a menu voice

        Args:
            menu (str): a menu identifier
            choice (int): a menu entry index

        Returns:
            int: 0 for normal, 1 for going back to the main menu of EasyCloud,
                 2 for closing the whole application
        """
        if menu == "floating_ips":
            if choice == 5:  # Detach floating ip
                SimpleTUI.msg_dialog("Detach Floating IP", "Detaching an IP address from an instance is not supported on GCP: \n" +
                                     "an instance must always have an IP associated while running.\n" +
                                     "However, you can assign another address to the desired instance to\n" +
                                     "replace the current one.",
                                     SimpleTUI.DIALOG_INFO)
                return 0
            SimpleTUI.error("Unavailable choice!")
            SimpleTUI.pause()
            SimpleTUI.clear_console()
        pass

    # =============================================================================================== #
    #                                  RuleEngine Actions Definition                                  #
    # =============================================================================================== #

    # All the actions are defined in the actions.py file in the module directory
    # Note: the platform name passed on the decorator must be equal to this class name

    @bind_action("GCP", "clone")
    def clone_instance(self, instance_id):
        """
        Clone instance
        """
        GCPAgentActions.clone_instance(self, instance_id)

    @bind_action("GCPCloud", "alarm")
    def alarm(self, resource_id):
        """
        Trigger an alarm
        """
        GCPAgentActions.alarm(self, resource_id)
