from easycloud.common.gcp import GCPBaseConfManager


class GCPConfManager(GCPBaseConfManager):
    """
    Google Cloud Platform configuration manager.
    """

    def __init__(self, config_file=None):
        """
        Init method (object initialization)
        """
        super().__init__("gcp_googleapi", config_file)
