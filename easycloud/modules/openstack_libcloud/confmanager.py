from easycloud.common.openstack import OpenStackBaseConfManager

class OpenStackConfManager(OpenStackBaseConfManager):
    """
    OpenStack configuration manager
    """

    def __init__(self, config_file=None):
        """
        Init method (object initialization)
        """
        super().__init__("openstack_libcloud", config_file)

    #def read_login_data(self):
    #    """
    #    Read login data from settings.cfg
    #    """
    #    super().read_login_data();
    #    self.os_telemetry_metering  = self.get_parameter("openstack", "os_telemetry_metering", return_type=str)
