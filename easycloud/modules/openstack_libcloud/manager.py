"""
EasyCloud Chameleon Cloud Manager.
"""

import datetime

from openstack.config import loader

from easycloud.core.actionbinder import bind_action
from easycloud.core.compute import Instance, InstanceStatus
from easycloud.core.metamanager import MetaManager
from easycloud.modules.chameleon_openstacksdk.actions import ChameleonCloudAgentActions
from easycloud.modules.chameleon_openstacksdk.confmanager import ChameleonCloudConfManager
from easycloud.modules.chameleon_openstacksdk.monitor import ChameleonCloudMonitor
from easycloud.tui.simpletui import SimpleTUI
import logging
import openstack
import time


class OpenStackInstance(Instance):
    """ An OpenStack instance.

    See:
    - https://docs.openstack.org/openstacksdk/latest/user/resources/compute/v2/server.html#openstack.compute.v2.server.Server
    """

    _NODE_STATUS_MAP = {
        'BUILD': InstanceStatus.PENDING,
        'REBUILD': InstanceStatus.PENDING,
        'ACTIVE': InstanceStatus.RUNNING,
        'SUSPENDED': InstanceStatus.SUSPENDED,
        'SHUTOFF': InstanceStatus.STOPPED,
        'DELETED': InstanceStatus.TERMINATED,
        'QUEUE_RESIZE': InstanceStatus.PENDING,
        'PREP_RESIZE': InstanceStatus.PENDING,
        'VERIFY_RESIZE': InstanceStatus.RUNNING,
        'PASSWORD': InstanceStatus.PENDING,
        'RESCUE': InstanceStatus.PENDING,
        'REBOOT': InstanceStatus.REBOOTING,
        'HARD_REBOOT': InstanceStatus.REBOOTING,
        'SHARE_IP': InstanceStatus.PENDING,
        'SHARE_IP_NO_CONFIG': InstanceStatus.PENDING,
        'DELETE_IP': InstanceStatus.PENDING,
        'ERROR': InstanceStatus.ERROR,
        'UNKNOWN': InstanceStatus.UNKNOWN
    }

    def __init__(self, os_conn, os_instance):
        # Example:
        #       openstack.compute.v2.server.Server(OS-EXT-STS:task_state=None, addresses={'CH-820879-net': [{'OS-EXT-IPS-MAC:mac_addr': 'fa:16:3e:c5:d0:e6', 'version': 4, 'addr': '10.185.189.137', 'OS-EXT-IPS:type': 'fixed'}]}, links=[{'href': 'https://kvm.tacc.chameleoncloud.org:8774/v2.1/servers/a0a9cdbb-a8f6-42fc-af4b-0e6c68d70ec3', 'rel': 'self'}, {'href': 'https://kvm.tacc.chameleoncloud.org:8774/servers/a0a9cdbb-a8f6-42fc-af4b-0e6c68d70ec3', 'rel': 'bookmark'}], image={'id': '206874ef-3d93-43c6-bc37-7335478a27a7', 'links': [{'href': 'https://kvm.tacc.chameleoncloud.org:8774/images/206874ef-3d93-43c6-bc37-7335478a27a7', 'rel': 'bookmark'}]}, OS-EXT-SRV-ATTR:user_data=None, OS-EXT-STS:vm_state=stopped, OS-EXT-SRV-ATTR:instance_name=instance-00001a28, OS-EXT-SRV-ATTR:root_device_name=/dev/vda, OS-SRV-USG:launched_at=2020-04-20T12:02:29.000000, flavor={'ephemeral': 0, 'ram': 512, 'original_name': 'm1.tiny', 'vcpus': 1, '_extraspecs': {}, 'swap': 0, 'disk': 1}, id=a0a9cdbb-a8f6-42fc-af4b-0e6c68d70ec3, security_groups=[{'name': 'default'}], description=easycloud-047, user_id=520b8f26b6214b3d9b0fab8878e67e44, OS-EXT-SRV-ATTR:hostname=easycloud-047, OS-DCF:diskConfig=MANUAL, accessIPv4=, accessIPv6=, OS-EXT-SRV-ATTR:reservation_id=r-7wfc3f6v, OS-EXT-STS:power_state=4, OS-EXT-AZ:availability_zone=nova, config_drive=, status=SHUTOFF, OS-EXT-SRV-ATTR:ramdisk_id=, updated=2020-09-10T09:30:49Z, hostId=aa36609e24cc62db7565ad56156451578e6856b8b3e7c8e4cf8fa58f, OS-EXT-SRV-ATTR:host=c07-34, OS-SRV-USG:terminated_at=None, tags=[], key_name=sguazt _at_ wildcat, OS-EXT-SRV-ATTR:kernel_id=, locked=False, OS-EXT-SRV-ATTR:hypervisor_hostname=c07-34, name=easycloud-047, OS-EXT-SRV-ATTR:launch_index=0, created=2020-04-20T12:02:17Z, tenant_id=2c18b5d8ebfa4a08b603c151d967a04d, os-extended-volumes:volumes_attached=[], trusted_image_certificates=None, metadata={}, location=Munch({'cloud': 'chameleon', 'region_name': 'KVM@TACC', 'zone': 'nova', 'project': Munch({'id': '2c18b5d8ebfa4a08b603c151d967a04d', 'name': 'CH-820879', 'domain_id': 'default', 'domain_name': None})}))

        self._os_conn = os_conn
        self._os_inst = os_instance
        self._status = self._NODE_STATUS_MAP.get(os_instance.status, InstanceStatus.UNKNOWN)
        self._private_ips = []
        self._public_ips = []  # TODO
        for net_name, nics in os_instance.addresses.items():
            for nic in nics:
                self._private_ips.append(nic['addr'])

    @property
    def extra(self):
        return self._os_inst

    @property
    def handle(self):
        return self._os_inst

    @property
    def id(self):
        return self._os_inst.id

    @property
    def private_ips(self):
        return self._private_ips

    @property
    def public_ips(self):
        return self._public_ips

    @property
    def name(self):
        return self._os_inst.name

    @property
    def status(self):
        return self._status

    def destroy(self):
        self._os_conn.compute.delete_server(self._os_inst)

    def reboot(self):
        self._os_conn.compute.reboot_server(self._os_inst, reboot_type='HARD')

    def start(self):
        self._os_conn.compute.start_server(self._os_inst)
        # TODO: call "self._os_conn.compute.wait_for_server(self._os_inst, status='ACTIVE', wait = timeout)" to perform a synchronous version of this method (note, if timeout is None you should invoke this method without the "wait" parameter, catching the "ResourceTimeout" exception and, in case such an exception is thrown, calling again the "wait_for_server()" method)

    def stop(self):
        self._os_conn.compute.stop_server(self._os_inst)
        # TODO: call "self._os_conn.compute.wait_for_server(self._os_inst, status='ACTIVE', wait = timeout)" to perform a synchronous version of this method (note, if timeout is None you should invoke this method without the "wait" parameter, catching the "ResourceTimeout" exception and, in case such an exception is thrown, calling again the "wait_for_server()" method)


class ChameleonCloud(MetaManager):
    """
    EasyCloud Chameleon Cloud Manager.
    """

    def __init__(self, config_file=None, rule_file=None):
        super().__init__(rule_file=rule_file)
        self.platform_name = "Chameleon Cloud"
        self.conf = ChameleonCloudConfManager(config_file)
        self.cloned_instances = []
        # self.snapshots = None
        self.os_client = None
        self.connect()

    @property
    def handle(self):
        return self.os_client

    # =============================================================================================== #
    #                                Platform-specific client creation                                #
    # =============================================================================================== #

    def connect(self):
        """
        Connection to the endpoint specified in the configuration file
        """
        # Configura il logging
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        logger = logging.getLogger(__name__)

        try:
            # Load configurations from clouds.yaml
            cloud_config_loader = loader.OpenStackConfig(
                config_files=['easycloud/modules/chameleon_openstacksdk/clouds.yaml'])

            # fetch all cloud projects
            available_clouds = cloud_config_loader.get_all_clouds()

            if not available_clouds:
                raise ValueError("No cloud configurations found in the YAML file.")

            # cloud's menu selection
            cloud_name = self._select_cloud(available_clouds)

            # load config for selected cloud project
            cloud_config = cloud_config_loader.get_one_cloud(cloud_name)

            # Connection to Openstack with loaded parameters
            self.os_client = openstack.connection.Connection(config=cloud_config)
            logger.info(f"Connessione a OpenStack riuscita per il cloud '{cloud_name}'.")
        except Exception as e:
            logger.error("Errore nella connessione a OpenStack: %s", e)
            raise

    def _select_cloud(self, available_clouds):
        """
        Display a command-line menu for the user to select a cloud project.
        """
        print("Available cloud projects:")
        for i, cloud in enumerate(available_clouds, start=1):
            print(f"{i}. {cloud.name}")

        while True:
            try:
                choice = int(input("Select the cloud project by number: "))
                if 1 <= choice <= len(available_clouds):
                    return available_clouds[choice - 1].name
                else:
                    print(f"Please select a number between 1 and {len(available_clouds)}.")
            except ValueError:
                print("Invalid input. Please enter a number.")
    

    def list_instances(self):
        """ Returns a list of instances.

        Returns:
            list:    A list of ``easycloud.core.compute.Instance`` objects.
        """
        instances = []
        for server in self.os_client.compute.servers():
            instance = OpenStackInstance(self.os_client, server)
            instances.append(instance)
        return instances

    # =============================================================================================== #
    #                                  Platform-specific list printers                                #
    # =============================================================================================== #

    # Nothing here

    # =============================================================================================== #
    #                                         List builders                                           #
    # =============================================================================================== #

    def _platform_list_all_images(self):
        """
        List all available images with dynamic column handling.
        """
        self.images = self.os_client.list_images()
        table_body = []

        for i, image in enumerate(self.images, start=1):
            row = [
                i,  # Indice
                image.name,
                image.id,
                getattr(image, "status", "Unknown")  
            ]
            table_body.append(row)

        return table_body

    def _platform_list_all_availability_zones(self):
        """
        Print all available images
        Format: "ID", "Name", Zone State", "Region Name"
        """
        # Unimplemented, not required
        pass

    def _platform_list_all_instance_types(self):
        """
        Print all instance types
        Format: "ID", "Instance Type ID", "vCPUs", "Ram (GB)", "Disk (GB)"
        """
        self.instance_types = list(self.os_client.list_flavors())  # Convert generator to list
        i = 1
        table_body = []
        for instance_type in self.instance_types:
            # vCPUs number is not provided in any way, so a n/a is reported instead
            table_body.append([i, instance_type.name, "n/a", instance_type.ram / 1024, instance_type.disk])
            i = i + 1
        return table_body

    def _platform_list_all_security_groups(self):
        """
        Print all security groups
        Format: "ID", "SG name", "SG description"
        """
        # Fetch security group
        self.security_groups = list(self.os_client.network.security_groups())

        i = 1
        table_body = []
        for security_group in self.security_groups:
            table_body.append([i, security_group.name, security_group.description])
            i += 1
        return table_body

    def _platform_list_all_networks(self):
        """
        Print id and other useful informations of all networks available
        Format: "ID", "Network name", "Description"
        """
        self.networks = self.os_client.ex_list_networks()
        i = 1
        table_body = []
        for network in self.networks:
            table_body.append([i, network.name, network.id])
            i = i + 1
        return table_body

    def _platform_list_all_key_pairs(self):
        """
        Print all key pairs
        Format: "ID", "Key name", "Key fingerprint"
        """
        # Fetch the key pairs
        self.key_pairs = list(self.os_client.compute.keypairs())

        table_body = []
        for i, key_pair in enumerate(self.key_pairs, start=1):
            table_body.append([i, key_pair.name, key_pair.fingerprint])

        return table_body

    def _platform_list_all_instances(self):
        """
        Print instance id, image id, IP address and state for each active instance
        Format: "ID", "Instance Name", "Instance ID", "IP address", "Status", "Key Name", "Avail. Zone"
        """
        self.instances = self.list_instances()
        i = 1
        table_body = []
        for instance in self.instances:
            if len(instance.public_ips) > 0 and None not in instance.public_ips:
                table_body.append([i, instance.name, instance.id, ", ".join(instance.public_ips), instance.state,
                                   instance.extra["key_name"], instance.extra["availability_zone"]])
            else:
                table_body.append([i, instance.name, instance.id, "-", instance.state, instance.extra["key_name"],
                                   instance.extra["availability_zone"]])
            i = i + 1
        return table_body

    def _platform_list_all_volumes(self):
        """
        Print volumes alongside informations regarding attachments
        Format: "Volume ID", "Creation", "Size (GB)", "Attached To", "Status"
        """
        self.volumes = self.os_client.list_volumes()
        i = 1
        table_body = []
        for volume in self.volumes:
            created_at = datetime.datetime.strptime(volume.extra["created_at"], "%Y-%m-%dT%H:%M:%S.%f").strftime(
                "%b %d %Y, %H:%M:%S") + " UTC"
            if "attachments" in volume.extra and len(volume.extra["attachments"]) > 0:
                node = self.os_client.ex_get_node_details(volume.extra["attachments"][0]["server_id"])
                table_body.append([i, volume.name, volume.id, created_at, volume.size,
                                   node.name + " (" + volume.extra["attachments"][0]["device"] + ")", volume.state,
                                   volume.extra["location"]])
            else:
                table_body.append([i, volume.name, volume.id, created_at, volume.size,
                                   "- (-)", volume.state, volume.extra["location"]])
            i = i + 1
        return table_body

    def _platform_list_all_floating_ips(self):
        """
        Print ip and other useful information of all floating ip available
        Format: ""ID", "Public Ip", "Floating IP ID", "Associated Instance"
        """
        self.floating_ips = self.os_client.ex_list_floating_ips()
        i = 1
        table_body = []
        for floating_ip in self.floating_ips:
            if (floating_ip.node_id is not None):
                node = self.os_client.ex_get_node_details(floating_ip.node_id)
                if (node is not None):
                    table_body.append([i, floating_ip.ip_address, floating_ip.id, node.name, "n/a"])
                else:
                    table_body.append([i, floating_ip.ip_address, floating_ip.id, "Load Balancer", "n/a"])
            else:
                table_body.append([i, floating_ip.ip_address, floating_ip.id, "-", "n/a"])
            i = i + 1
        return table_body

    # =============================================================================================== #
    #                                Platform-specific list builders                                  #
    # =============================================================================================== #

    # Nothing here

    # =============================================================================================== #
    #                                       Actions and Menus                                         #
    # =============================================================================================== #

    def _platform_get_region(self):
        return self.conf.os_region

    def _platform_create_new_instance(self, instance_name, image, instance_type, monitor_cmd_queue=None):
        """
        Create a new instance using the OpenStack API
        Some specific steps are performed here:
            - Ask for security group
            - Ask for key pair
            - Instance creation summary

        Args:
            instance_name (str): The name of the instance
            image (<image_type>): The image to be used as a base system
            instance_type (<instance_type>): The VM flavor
        """
        # 5. Security Group
        security_group_index = SimpleTUI.list_dialog("Security Groups available",
                                                     self.print_all_security_groups,
                                                     question="Select security group")
        if security_group_index is None:
            return
        security_group = self.security_groups[security_group_index - 1]

        # 6. Key Pair
        key_pair_index = SimpleTUI.list_dialog("Key Pairs available",
                                               self.print_all_key_pairs,
                                               question="Select key pair")
        if key_pair_index is None:
            return
        key_pair = self.key_pairs[key_pair_index - 1]

        # 7. Reservation id required if using CHI@TACC or CHI@UC (Optional)
        # For details about the fields, please visit
        # https://developer.openstack.org/api-ref/compute/?expanded=create-server-detail
        scheduler_hints = {}
        if (self._is_barebone()):
            reservation_id = SimpleTUI.input_dialog("Blazar reservations",
                                                    question="Insert reservation UUID",
                                                    return_type=str,
                                                    regex="[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}")
            scheduler_hints["reservation"] = reservation_id

        # Creation summary
        print("\n--- Creating a new instance with the following properties:")
        print("- %-20s %-30s" % ("Name", instance_name))
        print("- %-20s %-30s" % ("Image", image.name))
        print("- %-20s %-30s" % ("Instance Type", instance_type.name))
        print("- %-20s %-30s" % ("Key Pair", key_pair.name))
        print("- %-20s %-30s" % ("Security Group", security_group.name))
        if (self._is_barebone()):
            print("- %-20s %-30s" % ("Reservation", reservation_id))

        # ask for confirm
        print("")
        if (SimpleTUI.user_yn("Are you sure?")):
            instance = self.os_client.compute.create_server(
                name=instance_name,
                image_id=image.id,
                flavor_id=instance_type.id,
                key_name=key_pair.name,
                networks=[{"uuid": self._get_network_id()}],
                # Assumendo che ci sia una funzione per ottenere il network ID
                security_groups=[{"name": security_group.name}],
                **({"scheduler_hints": scheduler_hints} if scheduler_hints else {})
            )
            if instance is None:
                return False
            # Instance Monitoring
            if monitor_cmd_queue is not None and self.is_monitor_running():
                monitor_cmd_queue.put({"command": "add", "instance_id": instance.id})
            return True

        return False

    def _get_network_id(self):
        """
        Retrieves the network ID to be used for instance creation.
        Assumes that there is only one network or returns the first one found.
        """
        networks = list(self.os_client.network.networks())
        if not networks:
            raise Exception("No networks available.")
        # Assuming you want the first available network
        return networks[0].id

    def _platform_instance_action(self, instance, action):
        """
        Handle an instance action with Openstack API
        """
        if action == "reboot":
            return instance.reboot()
        elif action == "delete":
            return instance.destroy()

    def _platform_get_instance_info(self):
        """
        Return a list of instances info
        """
        info = []
        for instance in self.os_client.compute.servers():
            info.append({"id": instance.id, "name": instance.name})
        return info

    def _platform_is_volume_attached(self, volume):
        """
        Check if a volume is attached to an instance

        Args:
            volume (<volume>): The volume to check

        Returns:
            bool: True if the volume is successfully attached, False otherwise
        """
        if (len(volume.extra["attachments"]) == 0):
            return False
        elif (volume.extra["attachments"][0]["server_id"] is None):
            return False
        return True

    def _platform_detach_volume(self, volume):
        """
        Detach a volume using the OpenStack API

        Args:
            volume (<volume>): The volume to detach

        Returns:
            bool: True if the volume is successfully detached, False otherwise
        """
        result = self.os_client.detach_volume(volume)
        if (result):
            while True:
                updated_volume = self.os_client.ex_get_volume(volume.id)
                if not self._platform_is_volume_attached(updated_volume):
                    break
                time.sleep(3)
        return result

    def _platform_delete_volume(self, volume):
        """
        Delete a volume using the OpenStack API

        Args:
            volume (<volume>): The volume to delete

        Returns:
            bool: True if the volume is successfully deleted, False otherwise
        """
        return self.os_client.destroy_volume(volume)

    def _platform_attach_volume(self, volume, instance):
        """
        Attach a volume using the OpenStack API
        Some specific steps are performed here:
            - Exposure point (e.g. /dev/sdb)

        Args:
            volume (<volume>): The volume to attach
            instance (<instance>): The instance where the volume is to be attached

        Returns:
            bool: True if the volume is attached successfully, False otherwise
        """
        # Ask for exposure point (the GNU/Linux device where this volume will
        # be available)
        exposure_point = SimpleTUI.input_dialog("Exposure point",
                                                question="Specify where the device is exposed, e.g. ‘/dev/sdb’",
                                                return_type=str,
                                                regex="^(/[^/ ]*)+/?$")
        if exposure_point is None:
            return
        return self.os_client.attach_volume(instance, volume, exposure_point)

    def _platform_create_volume(self, volume_name, volume_size):
        """
        Create a new volume using the OpenStack API

        Args:
            volume_name (str): Volume name
            volume_size (int): Volume size in GB

        Returns:
            bool: True if the volume is successfully created, False otherwise
        """
        return self.os_client.create_volume(name=volume_name, size=volume_size)

    def _platform_is_ip_assigned(self, floating_ip):
        """
        Check if a floating IP is assigned to an instance

        Args:
            floating_ip (<floating_ip>): The floating IP to check

        Returns:
            bool: True if the floating IP is assigned, False otherwise
        """
        if (floating_ip.node_id is not None):
            return True
        return False

    def _platform_detach_floating_ip(self, floating_ip):
        """
        Detach a floating IP using the OpenStack API

        Args:
            floating_ip (<floating_ip>): The floating IP to detach

        Returns:
            bool: True if the floating IP is successfully detached, False otherwise
        """
        node = self.os_client.ex_get_node_details(floating_ip.node_id)
        result = self.os_client.ex_detach_floating_ip_from_node(node, floating_ip)
        if (result):
            while True:
                updated_floating_ip = self.os_client.ex_get_floating_ip(floating_ip.ip_address)
                if not self._platform_is_ip_assigned(updated_floating_ip):
                    break
                time.sleep(3)
        return result

    def _platform_release_floating_ip(self, floating_ip):
        """
        Release a floating IP using the OpenStack API

        Args:
            floating_ip (<floating_ip>): The floating IP to release

        Returns:
            bool: True if the floating IP is successfully released, False otherwise
        """
        return self.os_client.ex_delete_floating_ip(floating_ip)

    def _platform_associate_floating_ip(self, floating_ip, instance):
        """
        Associate a floating IP to an instance using the OpenStack API

        Args:
            floating_ip (<floating_ip>): The floating IP to attach
            instance (<instance>): The instance where the floating IP is to be assigned

        Returns:
            bool: True if the floating IP is successfully associated, False otherwise
        """
        return self.os_client.ex_attach_floating_ip_to_node(instance, floating_ip)

    def _platform_reserve_floating_ip(self):
        """
        Reserve a floating IP using the OpenStack API
        """
        # public - CHI@TACC and CHI@UC
        # ext-net - OpenStack@TACC
        # WARNING: the following like is not working when a user ask for a new floating IP at KVM@TACC
        # return self.os_client.ex_create_floating_ip(ip_pool="public" if self._is_barebone() else "ext-net")
        # We force the ip_pool to be 'public' in order to make the creation of a new floating IP possible 
        # we suspect the the previon code line was working also for reservation mode.
        return self.os_client.ex_create_floating_ip(ip_pool="public")

    # =============================================================================================== #
    #                               Platform-specific Actions and Menus                               #
    # =============================================================================================== #

    def _is_barebone(self):
        """
        Detect if the selected region is using barebone instances or KVM
        """
        if self.conf.os_region in ["CHI@TACC", "CHI@UC"]:
            return True
        return False

    def _platform_extra_menu(self):
        """
        Print the extra Functions Menu (specific for each platform)
        """
        while (True):
            menu_header = self.platform_name + " Extra Commands"
            menu_subheader = ["Region: \033[1;94m" +
                              self._platform_get_region() + "\033[0m"]
            menu_items = ["Handle reservations (Not available in this release)" if self._is_barebone()
                          else "Handle reservations (Not available on KVM)",
                          "Back to the Main Menu"]
            choice = SimpleTUI.print_menu(menu_header, menu_items, menu_subheader)
            # if choice == 1 and self._is_barebone():  # Blazar menu
            if choice == 1:
                if self._is_barebone():
                    SimpleTUI.msg_dialog("Error", "Unimplemented functionality", SimpleTUI.DIALOG_ERROR)
                else:
                    SimpleTUI.msg_dialog("Reservations Manager",
                                         "Reservation manager is not available on OpenStack@TACC.\n" +
                                         "Please use one of the following regions:\n\n" +
                                         "- CHI@TACC (https://chi.tacc.chameleoncloud.org)\n" +
                                         "- CHI@UC (https://chi.uc.chameleoncloud.org)", SimpleTUI.DIALOG_INFO)
            # self.manage_reservations()
            elif choice == 2:  # Back to the main menu
                break
            else:
                SimpleTUI.msg_dialog("Error", "Unimplemented functionality", SimpleTUI.DIALOG_ERROR)

    # =============================================================================================== #
    #                                         Override methods                                        #
    # =============================================================================================== #

    override_main_menu = [6, 8, 9]
    override_ips_menu = []
    override_volumes_menu = []

    def _platform_is_overridden(self, menu, choice):
        """
        Check if a menu voice is overridden by this platform

        Args:
            menu (str): a menu identifier
            choice (int): a menu entry index

        Returns:
            bool: the override status (True/False)
        """
        if menu == "main" and choice in self.override_main_menu:
            if choice == 6 and self._is_barebone():  # Volumes on barebone are not supported
                return True
            # elif choice in [8, 9] and not self._is_barebone():  # Monitor is not available on KVM
            #    return True
        return False

    def _platform_override_menu(self, menu, choice):
        """
        Override a menu voice

        Args:
            menu (str): a menu identifier
            choice (int): a menu entry index

        Returns:
            int: 0 for normal, 1 for going back to the main menu of EasyCloud,
                 2 for closing the whole application
        """
        if menu == "main":
            if choice == 6:  # Volumes on barebone
                SimpleTUI.msg_dialog("Volumes Handler", "CHI@TACC and CHI@UC don't support volumes.\n" +
                                     "Please use KVM (https://openstack.tacc.chameleoncloud.org)",
                                     SimpleTUI.DIALOG_INFO)
                return 0
            elif choice in [8, 9]:  # Monitor on KVM
                SimpleTUI.msg_dialog("Monitoring",
                                     "Monitoring and Rule Management features are not available on OpenStack@TACC.\n" +
                                     "Please use one of the following regions:\n\n" +
                                     "- CHI@TACC (https://chi.tacc.chameleoncloud.org)\n" +
                                     "- CHI@UC (https://chi.uc.chameleoncloud.org)", SimpleTUI.DIALOG_INFO)
                return 0
            SimpleTUI.error("Unavailable choice!")
            SimpleTUI.pause()
            SimpleTUI.clear_console()

    # =============================================================================================== #
    #                                         Monitor creation                                        #
    # =============================================================================================== #

    def _platform_get_monitor(self, commands_queue, measurements_queue, metrics_file=None):
        """
        Create the Chameleon Cloud Resources Monitor

        Args:
            commands_queue (Queue): message queue for communicating with the main
                                    thread and receiving commands regarding the metrics
                                    to observe
            measurements_queue (Queue): message queue for sending measurements to
                                        the platform RuleEngine
            metrics_files (str): path to the metrics file

        Returns:
            MetaMonitor: the platform-specific monitor
        """
        self.instances = self.list_instances()
        for instance in self.instances:
            # print("Adding instance to monitor init: " + str({"command": "add", "instance_id": instance.id}))
            # logging.debug("Adding instance to monitor init: " + str({"command": "add", "instance_id": instance.id}))
            commands_queue.put({"command": "add", "instance_id": instance.id})
        return ChameleonCloudMonitor(conf=self.conf,
                                     commands_queue=commands_queue,
                                     measurements_queue=measurements_queue,
                                     metrics_file=metrics_file)

    # =============================================================================================== #
    #                                  RuleEngine Actions Definition                                  #
    # =============================================================================================== #

    # All the actions are defined in the actions.py file in the module directory
    # Note: the platform name passed on the decorator must be equal to this class name

    @bind_action("ChameleonCloud", "clone")
    def clone_instance(self, instance_id):
        """
        Clone instance
        """
        ChameleonCloudAgentActions.clone_instance(self, instance_id)

    @bind_action("ChameleonCloud", "alarm")
    def alarm(self, instance_id):
        """
        Trigger an alarm
        """
        ChameleonCloudAgentActions.alarm(self, instance_id)

