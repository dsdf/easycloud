"""
OpenStack monitor implementation (using Ceilometer/Gnocchi API)
"""

__author__ = "Davide Monfrecola, Stefano Garione, Giorgio Gambino, Luca Banzato"
__copyright__ = "Copyright (C) 2019"
__credits__ = ["Andrea Lombardo", "Irene Lovotti"]
__license__ = "GPL v3"
__version__ = "0.10.0"
__maintainer__ = "Luca Banzato"
__email__ = "20005492@studenti.uniupo.it"
__status__ = "Prototype"


import datetime
from easycloud.core.metamonitor import MetaMonitor
#from keystoneauth1 import exceptions as ks_exception
from keystoneauth1 import loading as ks_loading
#from keystoneauth1 import session as ks_session
#from keystoneclient.v3 import client as ks_client_v3
import logging
import pytz
from tzlocal import get_localzone


class OpenStackMonitor(MetaMonitor):

    def __init__(self, conf, commands_queue, measurements_queue):
        """
        Init method

        Args:
            conf (MetaConfManager): a configuration manager holding all the settings
                                    for the Monitor
            commands_queue (Queue): message queue for communicating with the main
                                    thread and receiving commands regarding the metrics
                                    to observe
            measurements_queue (Queue): message queue for sending measurements to
                                        the platform RuleEngine
        """
        super().__init__(conf, commands_queue, measurements_queue)

        if conf.os_telemetry_metering == 'ceilometerclient':
            self._client = OpenStackCeilometerClientDriver(conf, message_builder)
        else: # == 'gnocchi' [default]
            self._client = OpenStackGnocchiClientDriver(conf, message_builder)

        self._bind_generic_metric_to_getter(name="cpu_load",
                                            function=self._client.get_cpu_load_measures)
        self._bind_generic_metric_to_getter(name="memory_free",
                                            function=self._client.get_memory_free_measures)
        self._bind_generic_metric_to_getter(name="memory_used",
                                            function=self._client.get_memory_used_measures)

    @property
    def handle(self):
        return self._client.handle

    def connect(self):
        """
        Connect to Ceilometer and initialize its client object
        """
        self.client.connect()

    def _get_metric_values(self, instance_id, metric, granularity, limit):
        """
        Ceilometer measurements getter. Conversion to generic metric name is performed here through _build_message.

        Args:
            instance_id (str): The id of the instance to fetch the measurements
            metric (str): The *specific* metric name to get the values
            granularity (int): The granularity of the measurements fetched, expressed in seconds
            limit (int): The maximum number of measurements returned

        Returns:
            dict: A structure containing all the measurements for a metric
        """
        logging.debug("Fetching metric \"" + metric + "\" with granularity=" + str(granularity) + " and limit=" + str(limit))
        samples = []

        try:
            samples = self._client.get_metric_values(instance_id=instance_id,
                                                     metric=metric,
                                                     granularit=granularity,
                                                     limit=limit)
            logging.debug("MEASUREMENTS: " + str(samples))
        except Exception as exception:
            samples.append(self._error_sample(exception))
            logging.error("There was an error while fetching measurements:\n" +
                          str(type(exception)) + " -  " + str(exception) +
                          ". Please note that this is normal if you recently created a new instance.")

        return samples


class OpenStackCeilometerClientDriver():
    """
        OpenStack python-ceilometerclient driver.

        Note:
            python-ceilometerclient is no longer support and has been removed from OpenStack since OpenStack's
            Queens release.

        References:
        - https://docs.openstack.org/ceilometer/latest/
        - https://docs.openstack.org/python-ceilometerclient/latest/api.html
        - https://docs.openstack.org/ceilometer/pike/webapi/v2.html
    """

    def __init__(self, conf, message_builder):
        """
        Init method

        Args:
            conf (MetaConfManager): a configuration manager holding all the settings
                                    for the Monitor
            message_builder (function): function used for formatting metric values
        """
        __import__('ceilometer.client')
        self.conf_ = conf
        self.message_builder_ = message_builder
        self._client = None

    @property
    def handle(self):
        return self._client

    def connect(self):
        """
        Connect to Ceilometer and initialize its client object
        """
        loader = ks_loading.get_plugin_loader('password')
        auth = loader.load_from_options(auth_url=self.conf_.os_auth_url,
                                        username=self.conf_.os_username,
                                        password=self.conf_.os_password,
                                        project_id=self.conf_.os_project_id,
                                        user_domain_name="default")
        #sess = ks_session.Session(auth=_auth, verify=False)
        self._client = ceilometerclient.client.get_client("2", # v2 API
                                                          #session=sess)
                                                          auth=auth,
                                                          region_name=self.conf_.os_region)

    # For the complete metrics list available for each instance, please use the
    # res_viewer.py script available in the module main directory, after
    # filling all the required authentication parameters
    # You can also look here:
    #   https://docs.openstack.org/ceilometer/latest/admin/telemetry-measurements.html

    def get_cpu_load_measures(self, instance_id, granularity, limit):
        """
        Returns samples for the CPU load metric.

        Args:
            instance_id (str): The id of the instance to fetch the measurements
            granularity (int): The granularity of the measurements fetched, expressed in seconds
            limit (int): The maximum number of measurements returned

        Returns:
            dict: A structure containing all the measurements regarding CPU Load
        """
        (start_time, end_time) = get_time_window(granularity, limit)

        return self.get_measures(instance_id=instance_id,
                                 metric="cpu_util",
                                 start_time=start_time,
                                 end_time=end_time,
                                 limit=limit)

    def get_memory_free_measures(self, instance_id, granularity, limit):
        """
        Returns samples for the memory free metric.

        Args:
            instance_id (str): The id of the instance to fetch the measurements
            granularity (int): The granularity of the measurements fetched, expressed in seconds
            limit (int): The maximum number of measurements returned

        Returns:
            dict: A structure containing all the measurements regarding Memory Free
        """
        #Note, there is no 'memory free' metrics in ceilometer:

        (start_time, end_time) = get_time_window(granularity, limit)

        mem_tots = self.get_measures(instance_id=instance_id,
                                     metric="memory",
                                     start_time=start_time,
                                     end_time=end_time,
                                     limit=limit)
        mem_uses = self.get_measures(instance_id=instance_id,
                                     metric="memory.usage",
                                     start_time=start_time,
                                     end_time=end_time,
                                     limit=limit,)

        # Trim the longer sample list to the length of the shorter
        # FIXME: we should also consider the value of timestamp
        #        For instance, mem_uses may stop with older timestamps than mem_tots.
        if len(mem_tots) > len(mem_uses):
            mem_tots = mem_tots[-len(mem_uses):]
        elif len(mem_tots) < len(mem_uses):
            mem_uses = mem_uses[-len(mem_tots):]

        samples = []
        for i in range(0, len(mem_tots)):
            samples.append(self.message_builder_(timestamp=(mem_tots[i].timestamp+mem_uses[i].timestamp)/2, # take the avg timestamp
                                                 value=mem_tots[i].value-mem_uses[i].value,
                                                 unit=mem_tots[i].unit))
        return samples

    def get_memory_used_measures(self, instance_id, granularity, limit):
        """
        Returns samples for the memory used metric.

        Args:
            instance_id (str): The id of the instance to fetch the measurements
            granularity (int): The granularity of the measurements fetched, expressed in seconds
            limit (int): The maximum number of measurements returned

        Returns:
            dict: A structure containing all the measurements regarding Memory Used
        """
        (start_time, end_time) = get_time_window(granularity, limit)

        return self.get_measures(instance_id=instance_id,
                                 metric="memory.usage",
                                 start_time=start_time,
                                 end_time=end_time,
                                 limit=limit)

    def get_measures(self, instance_id, metric, start_time, end_time, limit):
        """
        Returns samples related to the given metric.

        Args:
            instance_id (str): The id of the instance to fetch the measurements
            metric (str): The *specific* metric name to get the values
            start_time (datetime): The starting time of the first sample
            end_time (datetime): The ending time of the last sample
            limit (int): The maximum number of measurements returned

        Returns:
            dict: A structure containing all the measurements for a metric
        """
        logging.debug("Ceilometer -- Fetching metric \"" + metric + "\" with granularity=" + str(granularity) + " and limit=" + str(limit))

        logging.debug("Instance ID: " + str(instance_id))
        # logging.debug("FETCHING METRIC " + metric + ": " + str(instance_resources))

        query = [
            dict(field='resource_id', op='eq', value=instance_id),
            dict(field='timestamp', op='ge', value=start_time.strftime("%Y-%m-%dT%H:%M:%S")),
            dict(field='timestamp', op='le', value=end_time.strftime("%Y-%m-%dT%H:%M:%S"))
        ]
        random_measurements_values = self._client.samples.list(meter_name=metric,
                                                               q=query,
                                                               limit=limit)
        # Sort all the data (sometimes data can be unrodered)
        random_measurements_values.sort(key=lambda x: x.timestamp)

        # Extract the most recent limit-values
        measurements_values = random_measurements_values[-limit:]

        # Add the measurements to the list that will be returned
        samples = []
        for value in measurements_values:
            samples.append(self.message_builder_(timestamp=value.timestamp,
                                                 value=value.volume,
                                                 unit=value.unit))
        return samples


class OpenStackGnocchiClientDriver():
    """
        OpenStack Gnocchi driver.

        References:
        - https://github.com/gnocchixyz/python-gnocchiclient
        - https://gnocchi.osci.io/index.html
    """

    def __init__(self, conf, message_builder):
        """
        Init method

        Args:
            conf (MetaConfManager): a configuration manager holding all the settings
                                    for the Monitor
            message_builder (function): function used for formatting metric values
        """
        __import__('gnocchiclient.client')
        self.conf_ = conf
        self.message_builder_ = message_builder
        self._client = None

    @property
    def handle(self):
        return self._client

    def connect(self):
        """
        Connect to Gnocchi and initialize its client object
        """
        loader = ks_loading.get_plugin_loader('password')
        auth = loader.load_from_options(auth_url=self.conf_.os_auth_url,
                                        username=self.conf_.os_username,
                                        password=self.conf_.os_password,
                                        project_id=self.conf_.os_project_id,
                                        user_domain_name="default")
        self._client = gnocchiclient.client.Client("1",
                                                   adapter_options={
                                                        "region_name": self.conf.os_region},
                                                        session_options={"auth": _auth})

    # For the complete metrics list available for each instance, please use the
    # res_viewer.py script available in the module main directory, after
    # filling all the required authentication parameters
    # You can also look here:
    #   https://docs.openstack.org/ceilometer/latest/admin/telemetry-measurements.html

    def get_cpu_load_measures(self, instance_id, granularity, limit):
        """
        Returns samples for the CPU load metric.

        Args:
            instance_id (str): The id of the instance to fetch the measurements
            granularity (int): The granularity of the measurements fetched, expressed in seconds
            limit (int): The maximum number of measurements returned

        Returns:
            dict: A structure containing all the measurements regarding CPU Load
        """
        (start_time, end_time) = get_time_window(granularity, limit)

        return self.get_measures(instance_id=instance_id,
                                 metric="load@load",
                                 start_time=start_time,
                                 end_time=end_time,
                                 limit=limit)

    def get_memory_free_measures(self, instance_id, granularity, limit):
        """
        Returns samples for the memory free metric.

        Args:
            instance_id (str): The id of the instance to fetch the measurements
            granularity (int): The granularity of the measurements fetched, expressed in seconds
            limit (int): The maximum number of measurements returned

        Returns:
            dict: A structure containing all the measurements regarding Memory Free
        """
        (start_time, end_time) = get_time_window(granularity, limit)

        return self.get_measures(instance_id=instance_id,
                                 metric="memory@memory.free",
                                 start_time=start_time,
                                 end_time=end_time,
                                 limit=limit)

    def get_memory_used_measures(self, instance_id, granularity, limit):
        """
        Returns samples for the memory used metric.

        Args:
            instance_id (str): The id of the instance to fetch the measurements
            granularity (int): The granularity of the measurements fetched, expressed in seconds
            limit (int): The maximum number of measurements returned

        Returns:
            dict: A structure containing all the measurements regarding Memory Used
        """
        (start_time, end_time) = get_time_window(granularity, limit)

        return self.get_measures(instance_id=instance_id,
                                 metric="memory@memory.used",
                                 start_time=start_time,
                                 end_time=end_time,
                                 limit=limit)

    def get_measures(self, instance_id, metric, start_time, end_time, limit):
        """
        Returns samples related to the given metric.

        Args:
            instance_id (str): The id of the instance to fetch the measurements
            metric (str): The *specific* metric name to get the values
            start_time (datetime): The starting time of the first sample
            end_time (datetime): The ending time of the last sample
            limit (int): The maximum number of measurements returned

        Returns:
            dict: A structure containing all the measurements for a metric
        """
        logging.debug("Gnocchi -- Fetching metric \"" + metric + "\" with granularity=" + str(granularity) + " and limit=" + str(limit))

        logging.debug("Instance ID: " + str(instance_id))
        # logging.debug("FETCHING METRIC " + metric + ": " + str(instance_resources))

        samples = []

        instance_resources = self._client.resource.get("generic", instance_id)
        logging.debug("Instance ID: " + str(instance_id))
        # logging.debug("FETCHING METRIC " + metric + ": " + str(instance_resources))
        random_measurements_values = self._client.metric.get_measures(instance_resources["metrics"][metric],
                                                                      start=start_time_utc,
                                                                      end=end_time_utc,
                                                                      granularity=granularity)
        # Sort all the data (sometimes data can be unrodered)
        random_measurements_values.sort(key=lambda x: x[0])
        # Extract the most recent limit-values
        measurements_values = random_measurements_values[-limit:]
        metric_unit = self._client.metric.get(instance_resources["metrics"][metric])["unit"]
        # Add the measurements to the list that will be returned
        for value in measurements_values:
            samples.append(self.message_builder_(timestamp=value[0],
                                                 value=value[2],
                                                 unit=metric_unit))

        return samples


def get_time_window(granularity, limit):
    # Define time interval for retrieving metrics
    now = datetime.datetime.now()
    start_time_local = now - datetime.timedelta(seconds=(granularity * limit) + granularity * 2)
    end_time_local = now
    local_tz = get_localzone()  # Detect current timezone
    # Convert local time to UTC
    start_time_no_tz = local_tz.localize(
        start_time_local, is_dst=None)  # No daylight saving time
    end_time_no_tz = local_tz.localize(
        end_time_local, is_dst=None)  # No daylight saving time
    # Final times
    start_time_utc = start_time_no_tz.astimezone(pytz.utc)
    end_time_utc = end_time_no_tz.astimezone(pytz.utc)

    return (start_time_utc, end_time_utc)
