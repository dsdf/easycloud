from easycloud.common.openstack import OpenStackBaseConfManager

class ChameleonCloudConfManager(OpenStackBaseConfManager):
    """
    Chamaleon Cloud configuration manager.
    """

    def __init__(self, config_file=None):
        """
        Init method (object initialization)
        """
        super().__init__("chameleon_openstacksdk", config_file)
