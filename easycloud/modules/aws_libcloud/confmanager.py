from easycloud.common.aws import AWSBaseConfManager


class AWSConfManager(AWSBaseConfManager):
    """
    Amazon Web Services configuration manager.
    """

    def __init__(self, config_file=None):
        """
        Init method (object initialization)
        """
        super().__init__("aws_libcloud", config_file)
