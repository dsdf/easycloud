[openstack]

# OpenStack Parameters
#
# API keys and default region definition. Keys can be obtained through the
# Chameleon Cloud OpenStack Dashboard under Project > API Access > Show Credentials.
# In os_auth_url you have to insert the "Authentication URL", in os_username and os_password you have
# to insert the username and password that you use to login into chameleon portal and, finally,
# os_project_name and os_project_id are self explained.

os_auth_url = 
os_username = 
os_password = 
# Project ID/Name can sometimes be referred as Tenant ID/Name
os_project_name = 
os_project_id = 
# Region can be RegionOne, CHI@TACC, CHI@UC, KVM@TACC, ...
os_region = 

[re_demo]

# Reservation ID for cloning an instance with the Agent
# You can find this ID under Reservation > Leases > 
# <Your Lease Name> > Reservations > id
demo_reservation_id = 

[options]

# Time in seconds between measurements fetches
monitor_fetch_period = 60

# Granularity of the measurements in seconds
# Note: minimum value is 60 seconds
granularity = 60

# How many measurements are retrieved for a single metric each time
window_size = 5

# Minimum number of measurements (1-window_size) positive to a rule
# that must be positive in order to trigger an action
minimum_positive = 3

# Tells what measures sinks to enable
# This option takes a comma-separated list of labels, each of which is used to identify a specific "subsection" of this file, providing options specific to the asssociated measures sink.
# For instance:
#   measures_sinks = FOO, BAR
#   [measure_sink.FOO]
#   module = <fully qualified module name containing the class that implements FOO's measures sink>
#   class = <the name of the class that implements FOO's measures sink>
#   FOO_option1 = ...
#   FOO_option2 = ...
#   [measure_sink.BAR]
#   module = <fully qualified module name containing the class that implements BAR's measures sink>
#   class = <the name of the class that implements BAR's measures sink>
#   BAR_option1 = ...
#   BAR_option2 = ...
#   BAR_option3 = ...
# As shown in the above example, each subsection *must* contain at least two special options, namely:
# - module, whose value represents the fully qualified module name containing the class that implements a given measures sink, and
# - class, whose value represents the name of the class that implements a given measures sink.
# Note, there is a special sink called "dummy" which does not perform any operation and it is usually used for testing purpose.
#measures_sinks = dummy
measures_sinks = activemq, cassandra, file, kafka, mongodb, prometheus, rabbitmq, redis

# Settings related to file measures sinks

[measures_sink.activemq]

module = core.measures_sink.activemq
class = ActiveMQSink
# A comma-separated list of broker servers; for instance: broker1:port1, broker2:port2, broker3:port3
# Default: localhost:61613 
brokers = localhost:61613
# The optional login user
# Default: none
user =
# The optional password of the login user
# Default: none
password =
# The ActiveMQ destination (either a queue or a topic) where to publish measures
# collected from cloud platforms.
# Defaut: easycloud
destination = easycloud
# The ActiveMQ destination type; it can be either 'queue' or 'topic'.
# Defaut: topic
destination_type = topic

[measures_sink.cassandra]

module = core.measures_sink.cassandra
class = CassandraSink
# A comma-separated list of contact points to try connecting for cluster discovery; for instance:
# host1, host2:port2, host3
# Default: localhost
contact_points = localhost
# The server-side port to which Cassandra is listening
# Default: 9042
port = 9042
# The maximum version of the Cassandra native protocol to use.
# If not specified, the maximum supported version will be used and possibly downgraded to the actual supported version.
# Default: max supported
protocol_version =
# The Cassandra keyspace where to collect measures.
# Default: easycloud
keyspace = easycloud
# Name of the Cassandra table (formerly known as column family), inside the
# above keyspace, where to collect measures.
# Note, the table name should not contain the keyspace qualifier
# Default: measures
table = measures

[measures_sink.dummy]

module = core.measures_sink.dummy
class = DummySink

[measures_sink.file]

module = core.measures_sink.file
class = CsvFileSink
# The top-level path where to store CSV files.
# Default: <sys-temp-dir>/easycloud
basepath = /tmp/easycloud

[measures_sink.kafka]

module = core.measures_sink.kafka
class = KafkaSink
# A comma-separated list of broker servers for instance: broker1:port1, broker2:port2, broker3:port3
# Default: localhost:9092 
brokers = localhost:9092
# The Kafka topic where to publish measures collected from cloud platforms
# Defaut: easycloud
topic = easycloud

[measures_sink.mongodb]

module = core.measures_sink.mongodb
class = MongoDBSink
# A comma-separated list of hostnames / mongodb URIs of MongoDB instances to connect to.
# host1, host2:port2, mongodb://user:password@host3:port3
# Default: localhost
hosts = localhost
# The server-side port to which MongoDB is listening
# Default: 27017
port = 27017
# The MongoDB database where to collect measures.
# Default: easycloud
db = easycloud
# Name of the MongoDB collection, inside the above databased, where to collect measures.
# Default: measures
collection = measures

[measures_sink.prometheus]

module = core.measures_sink.prometheus
class = PrometheusSink
# The pushgateway server: host:port
# Default: localhost:9091 
brokers = localhost:9091
# The Prometheus job where to publish measures collected from cloud platforms
# Defaut: easycloud
job = easycloud

[measures_sink.rabbitmq]

module = core.measures_sink.rabbitmq
class = RabbitMQSink
# The URL to the RabbitMQ server (see `URLParameters` at https://pika.readthedocs.io/en/stable/modules/parameters.html#urlparameters)
# Default: amqp://localhost:5672
url = amqp://localhost:5672
# The RabbitMQ exchange to which sending messages (see https://www.rabbitmq.com/tutorials/amqp-concepts.html)
# Default: easycloud
exchange = easycloud

[measures_sink.redis]

module = core.measures_sink.redis
class = RedisSink
# The URL to the Redis server (see `redis.from_url()` at https://redis-py.readthedocs.io/en/latest/)
# Note, if the URL specifies also the database, the username, and the password, the following configuration parameters ``db``, ``username`` and ``password`` will be ignored.
# Default: redis://localhost:6379/
url = redis://localhost:6379/
# The db parameter is the database number. You can manage multiple databases in Redis at once, and each is identified by an integer. The max number of databases is 16 by default
# Default: 0
db = 0
# The username parameter is the user name used to authenticate to Redis (see: https://redis.io/commands/auth).
# Default: none
username = 
# The password parameter is the password used to authenticate to Redis (see: https://redis.io/commands/auth).
# Default: none
password = 
